/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */
// var urlHome = '../../..';
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 config.language = 'fr';
	 //config.uiColor = '#AADC6E';

	// KCEDITOR	
	//var urlHome = '/projet-librairie/public';
	
	// var urlHome = '../..';
	config.filebrowserBrowseUrl = urlHome + '/kcfinder/browse.php?opener=ckeditor&type=files';
	config.filebrowserImageBrowseUrl = urlHome + '/kcfinder/browse.php?opener=ckeditor&type=images';
	config.filebrowserFlashBrowseUrl = urlHome + '/kcfinder/browse.php?opener=ckeditor&type=flash';
	config.filebrowserUploadUrl = urlHome + '/kcfinder/upload.php?opener=ckeditor&type=files';
	config.filebrowserImageUploadUrl = urlHome + '/kcfinder/upload.php?opener=ckeditor&type=images';
	config.filebrowserFlashUploadUrl = urlHome + '/kcfinder/upload.php?opener=ckeditor&type=flash';


// Toolbar configuration generated automatically by the editor based on config.toolbarGroups.
// config.toolbar = [
// 	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
// 	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
// 	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll' ] },
// 	{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
// 	'/',
// 	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
// 	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
// 	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
// 	{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
// 	'/',
// 	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
// 	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
// 	{ name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
// 	{ name: 'others', items: [ '-' ] },
// 	{ name: 'about', items: [ 'About' ] }
// ];

// Toolbar groups configuration.
config.toolbarGroups = [
	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
	// { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
	// { name: 'forms' },
	
	//'/',
	
	//{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	{ name: 'basicstyles', groups: [ 'basicstyles'] },
	
	// { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
	{ name: 'paragraph', groups: [ 'align'] },

	{ name: 'links' },
	{ name: 'insert' },

	//'/',
	
	{ name: 'styles' },
	{ name: 'colors' },
	{ name: 'tools' },
	// { name: 'others' },
	// { name: 'about' }
];


	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Source,Templates,PasteText,PasteFromWord,Subscript,Superscript,Anchor,Flash,Smiley,SpecialChar,PageBreak,Iframe,Styles,Format,ShowBlocks';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

};
