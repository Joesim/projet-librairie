/**
 * Classe pour l'utilisation de l'api Google Books et l'affichage des résultats
 *
 * Requires jQuery version 3.3.1 ou plus récent
 *
 * Auteur : Joffrey SIMONNET
 * 
 * Copyright 2018  
 */


$(function(){
    
var $startIndex = 0;
var $maxResults = 10;
var $afficher = '';
var $afficherLigne = '';

// ENVOYER UNE REQUETE VERS l'API GOOGLE BOOK et affiche les resultats dans la div.results
function sendRequestBook($url, $resultsDisplay, $isNext){
    $.get({
        url: $url,
        beforeSend: function(){
            (!$isNext) ? $('.results').slideUp(300) : '';
            (!$isNext) ? $afficher = ""             : '';
            (!$isNext) ? $afficherLigne = ""        : '';
        }
    })
        .done(function(response){
            
            var $books = response.items;
 
            var $nbResponses = response.totalItems;

            // CAS : LA RECHERCHE NE DONNE PAS DE RESULTAT
            if(typeof $books !== 'undefined'){
                var $nbBooks = $books.length;
            }else{
                $('.results').html("<p class='title is-5'>Désolé nous n'avons trouvé aucun résultat pour : " + $("input[name='title']").val() + ' ' + $("input[name='author']").val() + ' ' + $("input[name='publisher']").val() + "</p>");
                return;
            }
            
            
            
            // CAS : LA RECHERCHE DONNE DES RESULTATS
            var $ids = [];

            for(var $i=0; $i < $nbBooks; $i++){ 
                $ids.push($books[$i].id);
            }
            
            $afficherLigne += ( ($resultsDisplay == 'heart-stroke-search') && (!$isNext) ) ? ' &nbsp; - &nbsp; <strong>Selectionner votre coup de coeur</strong>' : '';
            for(var $book of $books){
                
                // ********* VALEURS SIMPLES POUR ADMIN
                var $titleAdmin         = $book.volumeInfo.title;

                var $publishedDateAdmin = $book.volumeInfo.publishedDate? $book.volumeInfo.publishedDate.substring(0, 10)    : '';
                var $publisherAdmin     = $book.volumeInfo.publisher    ? $book.volumeInfo.publisher        : '';
                var $authorsAdmin       = $book.volumeInfo.authors      ? $book.volumeInfo.authors[0]       : '';
                var $imageLinkAdmin     = $book.volumeInfo.imageLinks   ? $book.volumeInfo.imageLinks.thumbnail : '../../assets/images/nondispo.png';
                var $priceAdmin         = $book.saleInfo.listPrice ? $book.saleInfo.listPrice.amount : '';
                var $isDescriptionAdmin = $book.volumeInfo.description  ? '<i class="fas fa-check"></i>'  : '<i class="fas fa-times"></i>';
                var $descriptionAdmin   = $book.volumeInfo.description  ? $book.volumeInfo.description : 'Résumé non disponible';


                // ********* VALEURS AJUSTEES
                
                var $title          = $book.volumeInfo.title;

                var $publishedDate  = $book.volumeInfo.publishedDate ? ('<span class="detail-title">Publié le </span> : ' + $book.volumeInfo.publishedDate.substring(0, 10)) : '';
                var $publisher      = $book.volumeInfo.publisher     ? ('<span class="detail-title">&nbsp;&nbsp;&nbsp;chez </span> ' + $book.volumeInfo.publisher) : '';
                var $description    = $book.volumeInfo.description   ? ('<h4><span class="detail-title">Résumé : </span></h4>' + $book.volumeInfo.description) : '';
                
                if($book.volumeInfo.authors){
                    var $authorList = $book.volumeInfo.authors;
                    if($authorList.length == 1){
                        var $authors    = '<span class="detail-title">Auteur</span> : '  + $book.volumeInfo.authors[0];
                    }else{
                        var $authors    = '<span class="detail-title">Auteurs</span> : ' + $book.volumeInfo.authors[0];
                        for($i=1; $i<$authorList.length;$i++){
                            $authors += ', ' + $book.volumeInfo.authors[$i];
                        }
                    }
                }
                
                if($book.volumeInfo.categories){
                    var $categorieList = $book.volumeInfo.categories;
                    if($categorieList.length == 1){
                        var $categories    = '<span class="detail-title">Categorie</span> : '  + $book.volumeInfo.categories[0];
                    }else{
                        var $categories    = '<span class="detail-title">Categories</span> : ' + $book.volumeInfo.categories[0];
                        for($i=1; $i<$categorieList.length;$i++){
                            $categories += ', ' + $book.volumeInfo.categories[$i];
                        }
                    }
                }else{
                    $categories = '';
                }
                
                var $imageLink      = $book.volumeInfo.imageLinks   ? $book.volumeInfo.imageLinks.thumbnail : 'assets/images/nondispo.png';
                
                var $price          = $book.saleInfo.listPrice ? $book.saleInfo.listPrice.amount : '';
                var $priceCurrency  = $book.saleInfo.listPrice ? $book.saleInfo.listPrice.currencyCode : '';
                $priceCurrency      = $priceCurrency == 'EUR' ? '€' : '';
                var $priceTag       = $book.saleInfo.listPrice ? '<span class="detail-title">Prix</span> : ' + $price + $priceCurrency : '';
                
                var $isbns          = $book.volumeInfo.industryIdentifiers   ? $book.volumeInfo.industryIdentifiers : ''; 

                for(var $isbn of $isbns){
                    if($isbn.type == 'ISBN_13'){
                        var $gencodeAdmin = $isbn.identifier;
                        var $gencode = '<span class="detail-title">ISBN</span> : ' + $isbn.identifier;
                    }
                    if($isbn.type == 'OTHER'){
                        var $gencodeAdmin = '';
                        var $gencode = '';
                    }
                }
                var pathCommand = Routing.generate('command', {
                        'isbn': $gencodeAdmin,
                        'title': $titleAdmin,
                        'author': $authorsAdmin
                    });
                
                
                // AFFICHER UN BOUTON "Commander"
                var $display_button = ( ( userlevel == 'ROLE_ADMIN' || userlevel == 'ROLE_USER' || userlevel == 'ROLE_CONTRIBUTOR') && ($gencodeAdmin != '') ) ? '<a class="button button-home is-link command-button" href="' + pathCommand + '" title="Envoyer un mail à votre libraire pour commander ce livre, pour faciliter le suivis de la commande vos nom et email sont également transmis (mail automatique)" onclick="return confirm(\'Confirmez votre commande, votre librairie Mot à Mot va traiter votre demande\')" >Commander</a><div class="clearfix"></div>' : '';

                if($resultsDisplay == 'heart-stroke-search'){
                    // MISE EN PAGE SIMPLIFIEE DES RESULTATS PARTIE ADMIN 
                    $afficherLigne += 
                    `<tr class="select_this">
                        <td><img style="min-width:32px; height:48px;" src="${$imageLinkAdmin}"></td>
                        <td>${$gencodeAdmin}</td>
                        <td>${$titleAdmin}</td>
                        <td>${$publishedDateAdmin}</td>
                        <td>${$authorsAdmin}</td>
                        <td>${$publisherAdmin}</td>
                        <td>${$priceAdmin}</td>
                        <td>${$isDescriptionAdmin}</td>
                        <td style="display:none;">${$priceCurrency}</td>
                        <td style="display:none;">${$descriptionAdmin}</td>
                    </tr>`;
                }else{
                    // MISE EN PAGE DES RESULTATS PAGE COUPS DE COEUR 
                    $afficherLigne += 
                    `<div class="card">
                      <div class="card-content">
                        <div class="media">
                          <div class="media-left">
                            <figure class="image is-128x128 is-3by4">
                              <img src="${$imageLink}" alt="image de ${$title}">
                            </figure>
                          </div>
                          <div class="media-content">
                            <p class="title is-4">${$title}</p>
                            <small><p class="published is-6">${$publishedDate + $publisher}</p></small>
                            <small><p class="authors is-6">${$authors}</p></small>
                            <small><p class="categories is-6">${$categories}</p></small>
                            <small><p class="isbn13 is-6">${$gencode}</p></small>
                            <small><p class="isbn13 is-6">${$priceTag}</p></small>
                          </div>
                        </div>
                    
                        <div class="result-content">
                            ${$description}
                        </div>
                            ${$display_button}
                      </div>
                    </div>`;
                }
            }
            
            
            $startIndex     += $maxResults;
            
            // ON REMPLI LA VARIABLE $afficher

            $afficher   = '<small class="nbResponses"><i>' + $nbResponses + ' résultats</i></small>';
            if($resultsDisplay == 'heart-stroke-search'){
                $afficher += '<table  class="table is-striped is-hoverable"><thead><tr><th></th><th>Gencode</th><th>Titre</th><th>Parution</th><th>Auteur</th><th>Editeur</th><th>Prix</th><th title="Un résumé du livre est-il disponible?">Résumé</th></tr></thead><tbody>';
                $afficher += $afficherLigne;
                $afficher += '</tbody></table>';
                $afficher += '<div class="button is-success next-results" value="'+ $startIndex + '">Résultats suivants</div>';
            }else if($resultsDisplay == 'heart-stroke-public'){
                $afficher += $afficherLigne;
                $afficher += '<div class="button is-success next-results" value="'+ $startIndex + '">Résultats suivants</div>';
            }
            
            $('.results').html($afficher);
            

        })
        .fail(function(err){
            console.log('Erreur : ',err);
        })
        .always(function(xhr,statusText){
            $('.results').slideDown(300);
    });
}

// ************* RECHERCHE SIMPLE

$(".searchbar").submit(function(event){
    
    event.preventDefault();

    var $recherche = $("input[name='recherche']").val();
    
    if($recherche){
        
        $startIndex = 0;

        var $url = 'https://www.googleapis.com/books/v1/volumes?q=' + $recherche + '&maxResults=' + $maxResults + '&langRestrict=fr&printType=books&startIndex=' + $startIndex;
        var $resultsDisplay = 'heart-stroke-public';
        var $isNext         = false;
    
        sendRequestBook($url, $resultsDisplay, $isNext);
        
    }else{
        $('.results').slideUp(300);
        $('.results').html('');
        
    }
})




// ************* RECHERCHE AVANCEE par titre auteur et éditeur

$("button.advanced-search").click(function(event){
    
    event.preventDefault();

    var $titleInput = $("input[name='title']").val() != ''           ? ('+intitle:'  + $("input[name='title']").val())           : '';
    var $authorInput = $("input[name='author']").val() != ''         ? ('+inauthor:' + $("input[name='author']").val())          : '';
    var $publisherInput = $("input[name='publisher']").val() != ''   ? ('+inpublisher:' + $("input[name='publisher']").val() )   : '';
    
    if($titleInput || $authorInput || $publisherInput){
    
        $startIndex = 0;
    
        var $url = 'https://www.googleapis.com/books/v1/volumes?q=' + $titleInput + $authorInput + $publisherInput + '&maxResults=' + $maxResults + '&langRestrict=fr&printType=books&startIndex=' + $startIndex;
        var $resultsDisplay = 'heart-stroke-public';
        var $isNext         = false;
    
        sendRequestBook($url, $resultsDisplay, $isNext);
    
    }else{
        $('.results').slideUp(300);
        $('.results').html('');
        
    }
})


// ************* RECHERCHE AVANCEE par GENCODE

$("button.gencode-search").click(function(event){
    event.preventDefault();
    
    var $gencodeInput = $("input[name='gencode']").val() != ''  ? $("input[name='gencode']").val() : '';
    var $gencodeReq = $gencodeInput != ''                       ? ('+isbn:'  + $gencodeInput)      : '';

    if($gencodeInput.length === 13){
        var $url = 'https://www.googleapis.com/books/v1/volumes?q=' + $gencodeReq;
        var $resultsDisplay = 'heart-stroke-public';
        var $isNext     = false;
    
        sendRequestBook($url, $resultsDisplay, $isNext);
    }else{
        $('.results').slideUp(300);
        $('.results').html('');
        
    }
})




// ************* AFFICHER RESULTATS SUIVANTS
// ************* SELECTIONNER UNE LIGNE DU TABLEAU DES RESULTATS ADMIN

var baliseResults   = document.querySelector(".results");
var baliseNext      = document.querySelector(".next-results");
var baliseLigne     = document.querySelector(".select_this");

baliseResults.addEventListener("click", function (event){
    
    // SELECTIONNER UNE LIGNE DU TABLEAU RESULTATS ADMIN ET ENVOYER LES VALEURS DANS LES CHAMPS CACHES DU FORMULAIRE
    if(event.target.parentNode.getAttribute("class") == 'select_this'){
        var line = event.target.parentNode.childNodes;

        var image           = line[1].childNodes[0].getAttribute("src") == '../../assets/images/nondispo.png' ? '../../assets/images/nondispo.png' : line[1].childNodes[0].getAttribute("src");
        var gencode         = line[3].innerHTML;
        var title           = line[5].innerHTML;
        var publishedDate   = line[7].innerHTML;
        var authors         = line[9].innerHTML;
        var publisher       = line[11].innerHTML;
        var price           = line[13].innerHTML;
        var priceCurrency   = line[17].innerHTML;
        var description     = line[19].innerHTML;
        
        
        document.getElementById('heart_stroke_title').value         = title;
        document.getElementById('heart_stroke_imageLink').value     = image;
        document.getElementById('heart_stroke_isbn').value          = gencode;
        document.getElementById('heart_stroke_publishedDate').value = publishedDate;
        document.getElementById('heart_stroke_authors').value       = authors;
        document.getElementById('heart_stroke_publisher').value     = publisher;
        document.getElementById('heart_stroke_price').value         = price;
        document.getElementById('heart_stroke_priceCurrency').value = priceCurrency;
        document.getElementById('heart_stroke_description').value   = description;
        
        $('.results').slideUp(300);
        $('.addHeartstroke').slideUp(300);
        $('.addHeartStrokeForm').slideDown(300);
        $('.StepInfo1').css('display', 'none');
        $('.StepInfo2').css('display', 'none');
        $('.StepInfo3').css('display', 'inline-block');
        
        
        // AFFICHER LES DETAILS DU LIVRE SELECTIONNE
        var $details = 
        `<div class="card">
          <div class="card-content">
            <div class="media">
              <div class="media-left">
                <figure class="image is-128x128 is-3by4">
                  <img src="${image}" alt="image de ${title}">
                </figure>
              </div>
              <div class="media-content">
                <p class="title is-4">${title}</p>
                <small><p class="published is-6"><span class="detail-title">Publié le</span> : ${publishedDate}</p></small>
                <small><p class="published is-6"><span class="detail-title">Editeur</span> : ${publisher}</p></small>
                <small><p class="authors is-6"><span class="detail-title">Auteur</span> : ${authors}</p></small>
                <small><p class="isbn13 is-6"><span class="detail-title">ISBN</span> : ${gencode}</p></small>
                <small><p class="isbn13 is-6"><span class="detail-title">Prix</span> : ${price + priceCurrency}</p></small>
              </div>
            </div>
          </div>
        </div>`;
        
        $('#heartstroke-details').html($details);
        $('admin-container-new').html($details);
    }

    // SELECTIONNER LE BOUTON AFFICHER RESULTATS SUIVANTS
    if(event.target.getAttribute("value") == $startIndex){
        var $simple_search_input            = $("input.simple-search").val()                != '' ? $("input.simple-search").val()                                  : '';
        var $advanced_search_title_input    = $("input.advanced-search-title").val()        != '' ? ('+intitle:'  + $("input.advanced-search-title").val())         : '';
        var $advanced_search_author_input   = $("input.advanced-search-author").val()       != '' ? ('+inauthor:' + $("input.advanced-search-author").val())        : '';
        var $advanced_search_publisher_input= $("input.advanced-search-publisher").val()    != '' ? ('+inpublisher:' + $("input.advanced-search-publisher").val() ) : '';
        
        
        var $search = "";
        if($advanced_search_title_input || $advanced_search_author_input || $advanced_search_publisher_input){
            $search = $advanced_search_title_input  + $advanced_search_author_input + $advanced_search_publisher_input;
        }else if($simple_search_input){
            $search = $simple_search_input;
        }else{
            return;
        }
        
        var $resultsDisplay = "";
        if($('.formLocation').text() == 'public'){
            $resultsDisplay = 'heart-stroke-public';
        }else if($('.formLocation').text() == 'admin'){
            $resultsDisplay = 'heart-stroke-search';
        }else{
            $resultsDisplay = "";
        }

        var $url = 'https://www.googleapis.com/books/v1/volumes?q=' + $search + '&maxResults=' + $maxResults + '&langRestrict=fr&printType=books&startIndex=' + $startIndex;
        var $isNext = true;
        
        sendRequestBook($url, $resultsDisplay, $isNext);
    }
});





// ************* AFFICHER LE FORMULAIRE DE RECHERCHE AVANCEE

$(".advancedResearchButton").click(function(event){
    event.preventDefault();
    $('.advancedResearch').slideToggle(200);
})




// ******************************************************************
// ******************** PARTIE ADMIN
// ******************************************************************

// ************* RECHERCHE par titre auteur et éditeur et affichage simplifié pour admin

$("button.heart-stroke-search").click(function(event){
    event.preventDefault();

    var $titleInput = $("input[name='title']").val() != ''           ? ('+intitle:'  + $("input[name='title']").val())           : '';
    var $authorInput = $("input[name='author']").val() != ''         ? ('+inauthor:' + $("input[name='author']").val())          : '';
    var $publisherInput = $("input[name='publisher']").val() != ''   ? ('+inpublisher:' + $("input[name='publisher']").val() )   : '';
    
    $startIndex = 0;

    var $url = 'https://www.googleapis.com/books/v1/volumes?q=' + $titleInput + $authorInput + $publisherInput + '&maxResults='+$maxResults+'&langRestrict=fr&printType=books&startIndex=' + $startIndex;
    var $resultsDisplay = 'heart-stroke-search';
    var $isNext = false;

    sendRequestBook($url, $resultsDisplay, $isNext);
})


// ************* RECHERCHE par gencode et affichage simplifié pour admin

$("button.heart-stroke-gencode-search").click(function(event){
    event.preventDefault();

    var $gencodeInput = $("input[name='gencode']").val() != ''           ? ('+isbn:'  + $("input[name='gencode']").val())           : '';

    var $url = 'https://www.googleapis.com/books/v1/volumes?q=' + $gencodeInput;
    var $resultsDisplay = 'heart-stroke-search';
    var $isNext = false;

    sendRequestBook($url, $resultsDisplay, $isNext);
})




})