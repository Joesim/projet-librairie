/**
 * Classe pour l'affichage des coups de coeurs avec AJAX
 *
 * Requires jQuery version 3.3.1 ou plus récent
 *
 * Auteur : Joffrey SIMONNET
 * 
 * Copyright 2018  
 */
console.log('begin js');

$(function(){
    
    $('.nextHeartstrokesButton, .close_cross').click(function (event) {
        
        event.preventDefault();
        
        if(this.className.search('nextHeartstrokesButton') != -1){ // CLIQUER SUR LE BOUTON AFFICHER
            var $heartstrokeContent = event.target.parentNode.parentNode.childNodes[3];
            var catId               = event.target.nextElementSibling.textContent;
            var year                = event.target.nextElementSibling.nextElementSibling.childNodes[1].value;
            event.target.parentNode.parentNode.childNodes[1].childNodes[1].innerHTML = '(' + year + ')';
            event.target.parentNode.parentNode.childNodes[1].childNodes[3].style.display = 'inline-block';
            
            $('.categoryId_'+ catId).animate({width: '100%'});
        }else if(this.className.search('close_cross') != -1){ // CLIQUER SUR LA CROIX POUR FERMER
            var $heartstrokeContent = event.target.parentNode.parentNode.childNodes[3];
            var catId               = event.target.parentNode.parentNode.childNodes[5].childNodes[5].textContent;
            var year                = 'last_three';
            event.target.previousElementSibling.innerHTML = '';
            event.target.style.display = 'none';
            
            $('.categoryId_'+ catId).animate({width: '45%'});
        }

        console.log(catId + year);

        if (catId && year) {
                $.ajax({
                    type: 'POST',
                    url: urlAjax,
                    data: {
                        "catId": catId,
                        "year" : year
                    },
                    success: function (data) {
                        console.log('success');
                        // event.target.previousElementSibling.innerHTML = data;
                        // var $baliseResult = event.target.previousElementSibling;
                        var $HTMLresult   = ''; 
                        
                        $.each(data, function (index, value) {
                            
                            var pathCommand = Routing.generate('command', {
                                'isbn': value[12],
                                'title': value[1],
                                'author': value[9]
                            });
                            

                            // AFFICHER UN BOUTON "Commander"
                            var $display_button = ( userlevel != 'not_connected' ) ? '<a class="button button-home is-link heartstroke-command" href="' + pathCommand + '" title="Envoyer un mail à votre libraire pour commander ce livre, pour faciliter le suivis de la commande vos nom et email sont également transmis (mail automatique)" onclick="return confirm(\'Confirmez votre commande, votre librairie Mot à Mot va traiter votre demande\')" >Commander</a><div class="clearfix"></div>' : '';

        
                            $HTMLresult += 
                            
                            `<div class='parent'>
                            
                                <div class="coverAndTitle">
                                    <img src="${value[6]}" id="myBtn_${value[0]}" class="myBtn heartstroke-img"><br>
                                    <small>${value[1]}</small>
                                </div>
                                
                                <div id="myModal_${value[0]}" class="modal">
                                    <div class="modal-content">
                                        <section class="modal-heartstroke">
                                            <div class="modal-heartstroke-head">
                                                <span class="close">&times;</span>
                                                <h2>${value[1]}</h2>
                                                <h3>${value[9]}</h3>
                                            </div>
                                            <div class="modal-heartstroke-body">
                                                <figure>
                                                    <img src="${value[6]}" alt="Couverture de ${value[1]}">
                                                </figure>
                                                <article>
                                                    <small><span class="detail-title">Sortie </span> ${value[7]}</small><br>
                                                    <small><span class="detail-title">Editeur </span> ${value[8]}</small><br>
                                                    <small><span class="detail-title">ISBN </span> ${value[12]}</small><br>
                                                    <small><span class="detail-title">Prix </span> ${value[10]} ${value[11]}</small><br>
                                                    <blockquote>
                                                        <span class="quote-title">Avis de ${value[4]} :</span> ${value[2]}  <br> 
                                                    </blockquote>
                                                </article>
                                            </div>
                                            <div class="modal-heartstroke-description">
                                                <span class="quote-title">Résumé :</span> ${value[5]}
                                            </div>
                                            ${$display_button}
                                        </section>
                                    </div>
                                </div>
                                
                            </div>`;
                            


                        });
                        
                        $heartstrokeContent.innerHTML = $HTMLresult;
                        
                        
                                                            

                        // if(this.className.search('nextHeartstrokesButton') != -1){ // CLIQUER SUR LE BOUTON AFFICHER
                        //     console.log('afficher');
                        //     $('.categoryId_'+ catId).animate({width: '100%'});
                        // }else if(this.className.search('close_cross') != -1){ // CLIQUER SUR LA CROIX POUR FERMER
                        //     console.log('close');
                        //     $('.categoryId_'+ catId).animate({width: '50%'});
                        // }
                        // $('.categoryId_'+ catId).animate({width: '100%'});
                    
                        
                        // *********************** CHARGEMENT DU SCRIPT POUR LES MODALES
                        $.getScript(urlModalJs);
                        
                        
                        return;
                    },
                    complete : function(){
                        console.log('complete');
                        // event.target.style.display = "none";
                    }
                });
        }
    })
    

    
    // //   if($(event.target).get(0).nodeName == 'BUTTON'){
    // //     // Récupère la liste des id ainsi que l'id à supprimer
    // //     $ingredientIdList = $('#ingredients_id').val();
    // //     $ingredientId = event.target.name.split('_')[1];
      
    // //     // Remplacer par la nouvelle liste dans le champs caché
    // //     $('#ingredients_id').val($ingredientIdList.replace( "_" + $ingredientId + "_", ""));
      
    // //     // Réactiver le nom de l'ingrédient dans le SELECT de tous les ingrédients pour pouvour le sélectionner à nouveau
    // //     $('#ingredients_list option[id="ingredient_' + $ingredientId + '"]').prop("disabled", false);
      
    // //     // Supprimer la ligne l'ingrédient avec quantité et unité
    // //     $(event.target).parent().remove();
    // //   }
    // });
    // console.log('clac');
})


