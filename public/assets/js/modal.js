/**
 * Classe pour l'affichage de fenetres modales de la page coups de coeur
 *
 * Requires jQuery version 3.3.1 ou plus récent
 *
 * Auteur : Joffrey SIMONNET
 * 
 * Copyright 2018  
 */

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Get the modals
var modals = document.getElementsByClassName('modal');
var modalTab = [];
for(i=0; i<modals.length; i++)
{
    var modalTemp = modals[i].id;
    modalTab.push( document.getElementById(modalTemp) );
}

// Get the button that opens the modal
var btns = document.getElementsByClassName("myBtn");

// Get the <span> element that closes the modal
var spans = document.getElementsByClassName("close");

// When the user clicks the button, open the modal
for(var i = 0; i <btns.length; i++){
    var btn = btns[i];
    var span= spans[i];

    btn.onclick = function(event) {
        var myWindow = event.target.parentNode.parentNode.childNodes[3];
        myWindow.classList.remove("closing");
        myWindow.style.display = "block";
    }
    
    span.onclick = function(event) {
        var myWindow =event.target.parentNode.parentNode.parentNode.parentNode;
        myWindow.classList.add("closing");
    }
    
    window.onclick = async function(event) {
        if (event.target.classList[0] == 'modal') {
            event.target.classList.add("closing");
        }
    }
}



// [].forEach.call(btns, function(image) {
//   image.addEventListener("click", function() {
//     modal.classList.toggle("off");
//   })
// });
