function ValidatePhone(phone){
    var tel = $(phone).val(); 
    if (tel.match(/^0[1-9]\d{8}$/) || tel.match(/^[1-9]\d{8}$/) || tel == ''){ 
       $('.tel_result').html("");
        return true;
    }
    else{
        $('.tel_result').html("Le téléphone doit être de type 0123456789 (10 chiffres), ou 123456789 (9 chiffres sans le premier zéro) ou vide");
        $('.tel_result').css("color", "red");
        $('.tel_result').css("fontWeight", "normal");
        return false;
    }        
}

$(function(){
    //Validation profil utilisateur --> verification des deux mot de passe entrés
    $("#user_profile_save").on("click",function(e){
        var mdP = $('#user_profile_hash').val(); 
        var mdPConfirm = $('#Conf_MdP').val(); 

        if (mdP === mdPConfirm)
        {
             $('#mdP_result').html("");
        }
        else{
            $('#mdP_result').html("Le mot de passe et la confirmation du mot de passe ne sont pas identiques");
            $('#mdP_result').css("color", "red");
            $('#mdP_result').css("fontWeight", "normal");
            e.preventDefault();
        }
        
        //Validation profil utilisateur --> verification du téléphone
        if(!ValidatePhone('#user_profile_phone')){
            e.preventDefault();
        }
    });
    
    //Validation admin edition --> verification du téléphone
    $("#user_admin_edit_save").on("click",function(e){
        if(!ValidatePhone('#user_admin_edit_phone')){
            e.preventDefault();
        }
    });
    
    //Validation admin création d'un nouvel utilisateur --> verification du téléphone
    $("#user_save").on("click",function(e){
        if(!ValidatePhone('#user_phone')){
            e.preventDefault();
        }
    });
    
    //Propose un nom d'utilisateur à la saisie du nom et prénom
    $("#user_lastName, #user_firstName").on("keyup",function(e){
        var firstnameVal = $('#user_firstName').val();
        var lastnameVal = $('#user_lastName').val();
        $('#user_username').val(firstnameVal + ' ' + lastnameVal);
    });
    
    $('#heart_stroke_edit_category option[value=""]').css("display", "none");
});