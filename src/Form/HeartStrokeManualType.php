<?php

namespace App\Form;

use App\Entity\HeartStroke;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class HeartStrokeManualType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // TRANSFORME LE REPO DES CATEGORIES EN UN TABLEAU ASSOCIATIF DES CATEGORIES
        $categoryList = $options['categories'];
        $categoryListName = [];
        foreach($categoryList as $category){
            $categoryListName[$category->getName()] = $category;
        }
        
        // TRANSFORME LE REPO DES Utilisateurs ayant le ROLE_admin EN UN TABLEAU ASSOCIATIF DES ADMINISTRATEURS DU SITE
        $userList = $options['users'];
        $adminListName = [];
        foreach($userList as $user){
            if($user->getUserLevel() == 'ROLE_ADMIN' && $user->getUsername() != 'Admin'){
                $adminListName[$user->getUsername()] = $user;
            }
        }
        
        $builder
            ->add('publishedDate'   , TextType::class, [
                'label' => 'Date de publication',
                'attr' => [
                    'placeholder' => "2018-01-31",
                    'class' => "input"
                    ]
                ])
            ->add('publisher'       , TextType::class, [
                'label' => 'Editeur',
                'attr' => [
                    'placeholder' => "Nom de l'éditeur",
                    'class' => "input"
                    ]
                ])
            ->add('description'     , TextareaType::class, [
                'label' => 'Résumé',
                'attr' => [
                    'placeholder' => "Résumé du livre",
                    'class' => "textarea"
                    ]
                ])
            ->add('authors'         , TextType::class, [
                'label' => 'Auteurs',
                'attr' => [
                    'placeholder' => "Nom de l'auteur",
                    'class' => "input"
                    ]
                ])
            ->add('price'           , TextType::class, [
                'label' => 'Prix',
                'attr' => [
                    'placeholder' => "5,99",
                    'class' => "input"
                    ]
                ])
            ->add('priceCurrency'   , TextType::class, [
                'label' => 'Devise',
                'attr' => [
                    'class' => "input",
                    'value' => "€"
                    ]
                ])
            ->add('isbn'            , TextType::class, [
                'label' => 'ISBN',
                'attr' => [
                    'placeholder' => "9782864249917",
                    'class' => "input"
                    ]
                ])
            ->add('imageLink'       , TextType::class, [
                'label' => 'Url de l\image',
                'attr' => [
                    'placeholder' => "https://image.jpg",
                    'class' => "input"
                    ]
                ])
            ->add('title'           , TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'placeholder' => "Titre du livre",
                    'class' => "input"
                    ]
                ])
            ->add('opinion', TextareaType::class, [
                'label' => 'Avis',
                'attr' => [
                    'placeholder' => "Votre avis sur le livre",
                    'class' => "textarea"
                    ]
                ])
            ->add('category', ChoiceType::class, [
                'label' => 'Catégorie',
                'attr' => [
                    'class' => 'select'
                ],
                'choices'  => $categoryListName
            ])
            
            ->add('user', ChoiceType::class, [
                'label' => 'Utilisateur',
                'attr' => [
                    'class' => 'select'
                ],
                'choices'  => $adminListName
            ])

            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
                'attr' => [
                    'class' => 'button is-success',
                    'title' => "Enregistrer le coup de coeur"
                ]
            ])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HeartStroke::class,
            'categories' => [],
            'users' => []
        ]);
    }
}
