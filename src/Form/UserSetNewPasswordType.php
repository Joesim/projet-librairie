<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserSetNewPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $token = $options['token'];

        $builder
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => "Votre adresse mail",
                    'class' => "input"
                    ]
                ])
            ->add('token', HiddenType::class, [
                'attr' => [
                    'value' => $token
                    ]
                ])
            ->add('password', PasswordType::class, [
                'attr' => [
                    'placeholder' => "Entrez votre nouveau mot de passe",
                    'class' => "input"
                    ]
                ])
            ->add('passwordCHK', PasswordType::class, [
                'attr' => [
                    'placeholder' => "Entrez votre nouveau mot de passe",
                    'class' => "input"
                    ]
                ])
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
                'attr' => [
                    'class' => 'button is-light',
                    ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // 'data_class' => User::class,
            'data_class' => null,
            'token'      => 'token_default'
        ]);
    }
}
