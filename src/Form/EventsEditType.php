<?php

namespace App\Form;

use App\Entity\Events;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use FOS\CKEditorBundle\Form\Type\CKEditorType;

class EventsEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => "Titre de l'évenement",
                    'class' => "input"
                    ]
                ])
                
            //KCFinder
            // ->add('content', TextareaType::class, [
            //     'required' => true,
            //     'attr' => [
            //         'placeholder' => "Contenu de l'évenement",
            //         'class' => "textarea"
            //         ]
            //    ])
                
             //ElFinder
            ->add('content', CKEditorType::class, [
                'config' => [
                    'config_name' => 'my_config',
                 ],
            ])
            
            ->add('save', SubmitType::class, [
                'label' => "Modifier",
                'attr' => [
                    'class' => 'button is-success'
               
                    ]
            ])
            ->add('saveAndAdd', SubmitType::class, [
                'label' => "Publier a nouveau",
                'attr' => [
                    'class' => 'button is-success',
                    'title' => "Enregistrer l'actualité"
                    ]
            ])
        ;
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Events::class,
        ]);
    }
}
