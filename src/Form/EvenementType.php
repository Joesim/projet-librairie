<?php

namespace App\Form;

use App\Entity\Evenement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use FOS\CKEditorBundle\Form\Type\CKEditorType;

class EvenementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => "Titre de l'évenement",
                    'class' => "input"
                    ]
                ])

             //ElFinder
            ->add('content', CKEditorType::class, [
                'config' => [
                    'config_name' => 'my_config',
                 ],
            ])
            
            ->add('created_at', DateType::class, array(
                'widget' => 'choice',
                'format' => 'dd-MM-yyyy',
                'years' => range(date('Y')-10, date('Y')+100),
                'months' => range(1, 12),
                'days' => range(1, 31),
                'placeholder' => array(
                    'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                )
                // 'months' => [0=>'a',1=>'z',2=>'e',3=>'r',4=>'t',5=>'y',6=>'u',7=>'i',8=>'o',9=>'p',10=>'q',11=>'s']
            ))
            
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
                'attr' => [
                    'class' => 'button is-success'
               
                    ]
            ])
        ;
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evenement::class,
        ]);
    }
}
