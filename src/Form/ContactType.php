<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $subjectList = [
            'Demande de renseignements' => 'Demande de renseignements',
            'Commander un livre' => 'Commander un livre'
        ];
        
        $builder
            ->add('username',TextType::class, [
                'label' => ' ',
                'attr' => [
                    'placeholder' => 'Votre nom d\'utilisateur',
                    'value' => $options['username'],
                    'style' => $options['display'],
                    'class' => "input"
                ]    
            ])
            ->add('email', EmailType::class, [
                'label' => ' ',
                'attr' => [
                    'placeholder' => 'adresse@email.fr',
                    'value' => $options['email'],
                    'style' => $options['display'],
                    'class' => 'input',
                ]
            ])
            ->add('lastName',HiddenType::class)
            ->add('firstName',HiddenType::class)
            ->add('title', ChoiceType::class, [
                'label' => 'Sujet',
                'choices'  => $subjectList
            ])
            ->add('content',TextareaType::class, [
                'attr' => [
                    'placeholder' => "Saisissez votre demande ici",
                    'class' => 'textarea is-hovered"'
                ]
            ])
            
            ->add('save', SubmitType::class, [
                'label' => "Nous écrire",
                'attr' => [
                    'class' => 'button is-light',
                    'title' => "Envoyer le message"
                    ]
            ])
          
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'username' => 'Votre nom d\'utilisateur',
            'email' => 'adresse@email.fr',
            'display' => '',
            'label' => ''
        ]);
    }
}
