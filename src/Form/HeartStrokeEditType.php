<?php

namespace App\Form;

use App\Entity\HeartStroke;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;


class HeartStrokeEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // TRANSFORME LE REPO DES CATEGORIES EN UN TABLEAU ASSOCIATIF DES CATEGORIES
        $categoryList = $options['categories'];
        $categoryListName = [];
        foreach($categoryList as $category){
            $categoryListName[$category->getName()] = $category;
        }
        
        // TRANSFORME LE REPO DES Utilisateurs ayant le ROLE_admin EN UN TABLEAU ASSOCIATIF DES ADMINISTRATEURS DU SITE
        $userList = $options['users'];
        $adminListName = [];
        foreach($userList as $user){
            if($user->getUserLevel() == 'ROLE_ADMIN' && $user->getUsername() != 'Admin'){
                $adminListName[$user->getUsername()] = $user;
            }
        }
        
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'placeholder' => "Titre du livre",
                    'class' => "input"
                    ]
                ])
                
             ->add('opinion', TextareaType::class, [
                'label' => 'Avis',
                'attr' => [
                    'placeholder' => "Votre avis sur le livre",
                    'class' => "textarea"
                    ]
                ]) 
                
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => [
                    'placeholder' => "Description du livre",
                    'class' => 'textarea'
                ]
            ])
            
            ->add('imageLink', UrlType::class, [
                'label' => 'URL de l\'image',
                'attr' => [
                    'placeholder' => "URL de l'image de couverture",
                    'class' => 'input'
                ]
            ])
            
            ->add('publishedDate', TextType::class, [
                'required' => false,
                'label' => 'Date de publication',
                'attr' => [
                    'placeholder' => "Date de publication",
                    'class' => "input"
                    ]
                ])
                
            ->add('publisher', TextType::class, [
                'required' => false,
                'label' => 'Editeur',
                'attr' => [
                    'placeholder' => "Editeur du livre",
                    'class' => "input"
                    ]
                ])
                
            ->add('authors', TextType::class, [
                'label' => 'Auteur(s)',
                'attr' => [
                    'placeholder' => "Auteur(s) du livre",
                    'class' => "input"
                    ]
                ])
                
            ->add('price', NumberType::class, [
                'required' => false,
                'label' => 'Prix',
                'scale' => 2,
                'attr' => [
                    'placeholder' => "Prix du livre",
                    'class' => "input"
                    ]
                ])
                
            ->add('priceCurrency', TextType::class, [
                'required' => false,
                'label' => 'Devise ',
                'attr' => [
                    'placeholder' => "Devise €, $...",
                    'class' => "input"
                    ]
                ])
                
            ->add('isbn', TextType::class, [
                'label' => 'ISBN ',
                'attr' => [
                    'placeholder' => "ISBN du livre",
                    'class' => "input"
                    ]
                ])
            
            ->add('category', ChoiceType::class, [
                'required' => false,
                'label' => 'Catégorie',
                'attr' => [
                    'class' => 'select'
                ],
                'choices'  => $categoryListName
            ])
            
            ->add('user', ChoiceType::class, [
                'label' => 'Utilisateur',
                'attr' => [
                    'class' => 'select'
                ],
                'choices'  => $adminListName
            ])

            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
                'attr' => [
                    'class' => 'button is-success',
                    'title' => "Enregistrer le coup de coeur"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HeartStroke::class,
            'categories' => [],
            'users' => []
        ]);
    }
}
