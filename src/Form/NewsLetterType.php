<?php

namespace App\Form;

use App\Entity\NewsLetter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

// use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class NewsLetterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'placeholder' => "Titre de la newsletter",
                    'class' => "input"
                    ]
                ])
            
            //KCFinder
            // ->add('content', TextareaType::class, [
            //     'attr' => [
            //         'placeholder' => "Contenu de la newsletter",
            //         'class' => "textarea"
            //         ]
            //     ])
            
            
            //ElFinder
            ->add('content', CKEditorType::class, [
                'config' => [
                    'config_name' => 'my_config',
                 ],
                 'required' => false
                ])
            
                
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
                'attr' => [
                    'class' => 'button is-success',
                    'title' => "Enregistrer la newsletter"
                    ]
            ]) 
        ;
         
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NewsLetter::class,
        ]);
    }
}
