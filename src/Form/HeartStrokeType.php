<?php

namespace App\Form;

use App\Entity\HeartStroke;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class HeartStrokeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // TRANSFORME LE REPO DES CATEGORIES EN UN TABLEAU ASSOCIATIF DES CATEGORIES
        $categoryList = $options['categories'];
        $categoryListName = [];
        foreach($categoryList as $category){
            $categoryListName[$category->getName()] = $category;
        }
        
        $builder
            ->add('publishedDate'   , HiddenType::class)
            ->add('publisher'       , HiddenType::class)
            ->add('description'     , HiddenType::class)
            ->add('authors'         , HiddenType::class)
            ->add('price'           , HiddenType::class)
            ->add('priceCurrency'   , HiddenType::class)
            ->add('isbn'            , HiddenType::class)
            ->add('imageLink'       , HiddenType::class)
            ->add('title'           , TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'class' => "input"
                    ]
                ])
            ->add('opinion', TextareaType::class, [
                'label' => 'Avis',
                'attr' => [
                    'placeholder' => "Votre avis sur le livre",
                    'class' => "textarea"
                    ]
                ])
            ->add('category', ChoiceType::class, [
                'label' => 'Catégorie',
                'attr' => [
                    'class' => 'select'
                ],
                'choices'  => $categoryListName
            ])

            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
                'attr' => [
                    'class' => 'button is-success',
                    'title' => "Enregistrer le coup de coeur"
                ]
            ])
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HeartStroke::class,
            'categories' => [],
            'users' => []
        ]);
    }
}
