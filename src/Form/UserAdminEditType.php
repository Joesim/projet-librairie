<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserAdminEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('username', TextType::class, [
                'attr' => [
                    'placeholder' => "Nom Utilisateur",
                    'class' => "input"
                ]    
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => "Adresse email",
                    'class' => 'input'
                ]
            ])
            ->add('userLevel', ChoiceType::class, [
                'choices'  => [
                    'Client' => 'ROLE_USER',
                    'Contributeur' => 'ROLE_CONTRIBUTOR',
                    'Administrateur' => 'ROLE_ADMIN'
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => "Nom",
                    'class' => "input",
                ]    
            ])
            ->add('firstName', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => "Prénom",
                    'class' => "input"
                ]
            ])  
            ->add('phone', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => "Téléphone",
                    'class' => "input"
                ]    
            ])
            ->add('address', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => "Adresse",
                    'class' => "input"
                ]    
            ])
            ->add('newsLetter',  CheckboxType::class, [
                    'label'    => 'Inscription à la newsletter ?',
                    'required' => false
                ]
            )
            ->add('save', SubmitType::class, [
                'label' => "Enregistrer",
                'attr' => [
                    'class' => 'button is-success',
                    'title' => "Enregistrer l'utilisateur"
                    ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
