<?php

namespace App\Repository;

use App\Entity\Events;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Events|null find($id, $lockMode = null, $lockVersion = null)
 * @method Events|null findOneBy(array $criteria, array $orderBy = null)
 * @method Events[]    findAll()
 * @method Events[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Events::class);
    }
    
    
    public function findNbEvents()
    {
        $qb = $this->createQueryBuilder('e')
                   ->select('COUNT(e.id) AS nbEvents' );
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
    public function getEventsPagination($limit, $offset)
    {
        $qb = $this->createQueryBuilder('e')
                   ->setFirstResult( $offset )
                   ->setMaxResults( $limit )
                   ->orderBy('e.createdAt', 'DESC')
                   ->getQuery();

        return $qb->execute();
    }
    
    public function getEventsByKeyword($keyword)
    {
        $qb = $this->createQueryBuilder('e')
            ->innerJoin('e.user', 'u', 'WITH')
            ->andWhere('e.title LIKE :keyword')
            ->orWhere('e.content LIKE :keyword')
            ->orWhere('e.createdAt LIKE :keyword')
            ->orWhere('u.lastName LIKE :keyword')
            ->orWhere('u.firstName LIKE :keyword')
            ->orWhere('u.username LIKE :keyword')
            ->orderBy('e.createdAt', 'DESC')
            ->setParameter('keyword', "%$keyword%")
            ->getQuery();

        return $qb->execute();
    }


    // RETOURNE LE DERNIER EVENEMENT OOU ACTUALIT2 PUBLIEE
    public function findLast()
    {
        return $this->createQueryBuilder('h')
            ->orderBy('h.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }

//    /**
//     * @return Events[] Returns an array of Events objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Events
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
