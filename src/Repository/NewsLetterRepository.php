<?php

namespace App\Repository;

use App\Entity\NewsLetter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NewsLetter|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsLetter|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsLetter[]    findAll()
 * @method NewsLetter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsLetterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NewsLetter::class);
    }
    
    public function findNbNewsLetters()
    {
        $qb = $this->createQueryBuilder('n')
                   ->select('COUNT(n.id) AS nbNewsLetter' );
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
    public function getNewsLettersPagination($limit, $offset)
    {
        $qb = $this->createQueryBuilder('n')
                   ->setFirstResult( $offset )
                   ->setMaxResults( $limit )
                   ->orderBy('n.createdAt', 'DESC')
                   ->getQuery();

        return $qb->execute();
    }
    
    public function getNewsLetterByKeyword($keyword)
    {
        $qb = $this->createQueryBuilder('n')
            ->innerJoin('n.user', 'u', 'WITH')
            ->andWhere('n.title LIKE :keyword')
            ->orWhere('n.content LIKE :keyword')
            ->orWhere('n.createdAt LIKE :keyword')
            ->orWhere('u.lastName LIKE :keyword')
            ->orWhere('u.firstName LIKE :keyword')
            ->orWhere('u.username LIKE :keyword')
            ->orderBy('n.createdAt', 'DESC')
            ->setParameter('keyword', "%$keyword%")
            ->getQuery();

        return $qb->execute();
    }

//    /**
//     * @return NewsLetter[] Returns an array of NewsLetter objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewsLetter
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
