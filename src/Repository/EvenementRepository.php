<?php

namespace App\Repository;

use App\Entity\Evenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Evenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evenement[]    findAll()
 * @method Evenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Evenement::class);
    }


    public function findNbEvenement()
    {
        $qb = $this->createQueryBuilder('e')
                   ->select('COUNT(e.id) AS nbEvenements' );
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
    public function findAllOrdered()
    {
        return $this->createQueryBuilder('e')
            ->orderBy('e.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
    
    public function findNext()
    {
        return $this->createQueryBuilder('e')
            ->where('e.createdAt >= CURRENT_DATE()')
            ->orderBy('e.createdAt', 'ASC')
            ->getQuery()
            ->getResult();
    }
    
    public function findPrev()
    {
        return $this->createQueryBuilder('e')
            ->where('e.createdAt < CURRENT_DATE()')
            ->orderBy('e.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
    
    public function getEvenementsPagination($limit, $offset)
    {
        $qb = $this->createQueryBuilder('e')
                   ->setFirstResult( $offset )
                   ->setMaxResults( $limit )
                   ->orderBy('e.createdAt', 'DESC')
                   ->getQuery();

        return $qb->execute();
    }
    
    public function getEvenementsByKeyword($keyword)
    {
        $qb = $this->createQueryBuilder('e')
            ->where('e.title LIKE :keyword')
            ->orWhere('e.content LIKE :keyword')
            ->orWhere('e.createdAt LIKE :keyword')
            ->orderBy('e.createdAt', 'DESC')
            ->setParameter('keyword', "%$keyword%")
            ->getQuery();

        return $qb->execute();
    }
}