<?php

namespace App\Repository;

use App\Entity\HeartStroke;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HeartStroke|null find($id, $lockMode = null, $lockVersion = null)
 * @method HeartStroke|null findOneBy(array $criteria, array $orderBy = null)
 * @method HeartStroke[]    findAll()
 * @method HeartStroke[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeartStrokeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HeartStroke::class);
    }
    
     public function findNbHeartStrokes()
    {
        $qb = $this->createQueryBuilder('h')
                   ->select('COUNT(h.id) AS nbHeartStroke' );
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
        
     public function findNbHeartStrokesByCategory($category)
    {
        $qb = $this->createQueryBuilder('h')
                   ->select('COUNT(h.id) AS nbHeartStroke' )
                   ->andWhere('h.category = :category')
                   ->setParameter('category', $category);
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
        
     public function findNbHeartStrokesByUser($user)
    {
        $qb = $this->createQueryBuilder('h')
                   ->select('COUNT(h.id) AS nbHeartStroke' )
                   ->andWhere('h.user = :user')
                   ->setParameter('user', $user);
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
    
    
     public function findNbHeartStrokesByCategoryByUser($user, $category)
    {
        $qb = $this->createQueryBuilder('h')
                   ->select('COUNT(h.id) AS nbHeartStroke' )
                   ->andWhere('h.user = :user')
                   ->andWhere('h.category = :category')
                   ->setParameter('user', $user)
                   ->setParameter('category', $category);
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
    
    public function getHeartStrokePagination($limit, $offset)
    {
        $qb = $this->createQueryBuilder('h')
                   ->setFirstResult( $offset )
                   ->setMaxResults( $limit )
                   ->orderBy('h.createdAt', 'DESC')
                   ->getQuery();

        return $qb->execute();
    }
    
    public function getHearthStrokeByKeyword($keyword)
    {
        $qb = $this->createQueryBuilder('h')
            ->innerJoin('h.user', 'u', 'WITH')
            ->innerJoin('h.category', 'c', 'WITH')
            ->andWhere('h.title LIKE :keyword')
            ->orWhere('h.opinion LIKE :keyword')
            ->orWhere('h.createdAt LIKE :keyword')
            ->orWhere('h.description LIKE :keyword')
            ->orWhere('h.authors LIKE :keyword')
            ->orWhere('h.publishedDate LIKE :keyword')
            ->orWhere('h.isbn LIKE :keyword')
            ->orWhere('u.lastName LIKE :keyword')
            ->orWhere('u.firstName LIKE :keyword')
            ->orWhere('u.username LIKE :keyword')
            
            ->orWhere('c.name LIKE :keyword')
            ->setParameter('keyword', "%$keyword%")
            ->orderBy('h.createdAt', 'DESC')
            ->getQuery();

        return $qb->execute();
        
    }



//   @return HeartStroke[] Returns an array of HeartStroke objects

    public function findByCategoryOrdered($category)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.category = :category')
            ->setParameter('category', $category)
            ->orderBy('h.publishedDate', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
        ;
    }
    
//   @return HeartStroke[] Returns an array of HeartStroke objects

    public function findAllByCategoryOrdered($category)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.category = :category')
            ->setParameter('category', $category)
            ->orderBy('h.publishedDate', 'DESC')
            ->getQuery()
            ->getResult();
    }
    
    
    //   @return HeartStroke[] Returns an array of HeartStroke objects
    //   Liste des coups de coeur d'une catégorie et d'une année selectionnée
    
    public function findByYearByCategoryOrdered($catId, $year)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.category = :category')
            ->andWhere('h.publishedDate LIKE :year')
            ->setParameter('category', $catId)
            ->setParameter('year', '%'.$year.'%')
            ->orderBy('h.publishedDate', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
                

    //   @return Years[] Returns an array of years
    //   Tableau des années des coups de coeur d'une catégorie
    
     public function findYearsByCategoryOrdered($category)
    {
        $qb = $this->createQueryBuilder('h')
                   ->select('h.publishedDate')
                   ->andWhere('h.category = :category')
                   ->setParameter('category', $category)
                   ->orderBy('h.publishedDate', 'DESC');
        return $qb->getQuery()
                  ->getArrayResult();
    }
    

    /*
    public function findOneBySomeField($value): ?HeartStroke
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
