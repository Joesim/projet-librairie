<?php

namespace App\Repository;

use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Contact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contact[]    findAll()
 * @method Contact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    public function findNbMessages()
    {
        $qb = $this->createQueryBuilder('m')
                   ->select('COUNT(m.id) AS nbMessages' );
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
    public function getMessagesPagination($limit, $offset)
    {
        $qb = $this->createQueryBuilder('m')
                   ->setFirstResult( $offset )
                   ->setMaxResults( $limit )
                   ->orderBy('m.createdAt', 'DESC')
                   ->getQuery();

        return $qb->execute();
    }
//    /**
//     * @return Contact[] Returns an array of Contact objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contact
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
