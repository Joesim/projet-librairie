<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }
    
   public function findNbUsers()
    {
        $qb = $this->createQueryBuilder('u')
                   ->select('COUNT(u.id) AS nbUser' );
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
    public function getUsersPagination($limit, $offset)
    {
        $qb = $this->createQueryBuilder('u')
                   ->setFirstResult( $offset )
                   ->setMaxResults( $limit )
                   ->orderBy('u.createdAt', 'DESC')
                   ->getQuery();

        return $qb->execute();
    }
    
    public function getUserByKeyword($keyword)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.lastName LIKE :keyword')
            ->orWhere('u.firstName LIKE :keyword')
            ->orWhere('u.username LIKE :keyword')
            ->orWhere('u.email LIKE :keyword')
            ->orWhere('u.address LIKE :keyword')
            ->orWhere('u.phone LIKE :keyword')
            ->orWhere('u.userLevel LIKE :keyword')
            ->orderBy('u.createdAt', 'DESC')
            ->setParameter('keyword', "%$keyword%")
            ->getQuery();

        return $qb->execute();
    }

    public function getUsersByStatut($statut)
    {
        return $this->createQueryBuilder('u')
                   ->select('u' )
                   ->where('u.userLevel = :statut')
                   ->setParameter('statut', $statut)
                   ->getQuery()
                   ->getResult();
    }

    public function getNbUsersByStatut($statut)
    {
        $qb = $this->createQueryBuilder('u')
                   ->select('COUNT(u.userLevel) AS nbUserLevel' )
                   ->where('u.userLevel = :statut')
                   ->setParameter('statut', $statut);
        return $qb->getQuery()
                  ->getSingleScalarResult();
    }
    
    // public function getUserByKeyword($keyword)
    // {
    //     $conn = $this->getEntityManager()->getConnection();
    
    //     $sql = '
    //         SELECT * FROM user
    //         WHERE first_name LIKE :keyword
    //         OR last_name LIKE :keyword
    //         OR email LIKE :keyword
    //         ';
    //     $stmt = $conn->prepare($sql);
    //     $stmt->execute(['keyword' => "%$keyword%"]);

    //     return $stmt->fetchAll();
    // }
 

//    /**
//     * @return User[] Returns an array of User objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
