<?php

namespace App\Controller;

use App\Entity\HeartStroke;
use App\Entity\Category;
use App\Form\HeartStrokeType;
use App\Form\HeartStrokeEditType;
use App\Form\HeartStrokeManualType;
use App\Repository\HeartStrokeRepository;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("admin/")
 * @Security("has_role('ROLE_CONTRIBUTOR') or has_role('ROLE_ADMIN') ")
 */
class AdminHeartStrokeController extends AdminController
{
    /**
     * @Route("heartstroke", name="admin_heartstroke", methods="GET|POST")
     */
    public function index(): Response
    {
    
        //On teste si l'admin effectue une recherche
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('heartstrokes');
            
            return $this->render('admin_heart_stroke/search.html.twig', [
                    'heartstrokes' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }else{
             
            //Sinon, on affiche toutes les coups de coeur de la première page
            $res = $this->pagination('heartstrokes');
            
            if(empty($res['resultat'])){
              $this->addFlash('danger', "Il n' y a aucun coup de coeur" );
            }
            
            return $this->render('admin_heart_stroke/index.html.twig', [
                    'heartstrokes' => $res['resultat'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
            }
    }

    /**
     * @Route("heartstroke/new", name="admin_heartstroke_new", methods="GET|POST")
     */
    public function new(Request $request, HeartStrokeRepository $repo, CategoryRepository $repoCategory): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('heartstrokes');
            return $this->render('admin_heart_stroke/search.html.twig', [
                    'heartstrokes' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        // RECUPERATION DES NOMS DES CATEGORIES SOUS FORME D'UN TABLEAU A PASSER EN OPTIONS AU FORMULAIRE
        $repoCategory   = $this->getDoctrine()->getRepository(Category::class);
        $categories     = $repoCategory->findAll();
        

        //CREATION DU FORMULAIRE
        $heartstroke = new HeartStroke();
        
        $form = $this->createForm(HeartStrokeType::class, $heartstroke, array(
                'categories' => $categories
            ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $user = $this->getUser();
            
            $data = $form->getData();
            $heartstroke->setCreatedAt(new \DateTime("now",new \DateTimezone("Europe/Paris")))
                        ->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($heartstroke);
            $em->flush();
            
            $this->addFlash('success', "Le coup de coeur a bien été ajouté!");

            return $this->redirectToRoute('admin_heartstroke');
        }

        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_heart_stroke/new.html.twig', [
            'heartstroke' => $heartstroke,
            'form' => $form->createView(),
        ]);
    } 



    // TRAITEMENT DU FORMULAIRE DE SAISIE MANUELLE D'UN COUP DE COEUR
    /**
     * @Route("heartstroke/newManually", name="admin_heartstroke_newManually", methods="GET|POST")
     */
    public function newManually(Request $request, HeartStrokeRepository $repo, CategoryRepository $repoCategory, UserRepository $repoUser): Response
    {
        // RECUPERATION DES NOMS DES CATEGORIES  SOUS FORME D'UN TABLEAU A PASSER EN OPTIONS AU FORMULAIRE
        $repoCategory   = $this->getDoctrine()->getRepository(Category::class);
        $categories     = $repoCategory->findAll();
        $users          = $repoUser->findAll();
        
        //CREATION DU FORMULAIRE
        $heartstroke = new HeartStroke();
        $form = $this->createForm(HeartStrokeManualType::class, $heartstroke, array(
            // DONNEES PASSEES EN OPTION 
            'categories' => $categories,
            'users'      => $users
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $heartstroke->setCreatedAt(new \DateTime("now",new \DateTimezone("Europe/Paris")));
            $em = $this->getDoctrine()->getManager();
            $em->persist($heartstroke);
            $em->flush();

            $this->addFlash('success', "Le coup de coeur a bien été ajouté!");

            return $this->redirectToRoute('admin_heartstroke');
        }

        return $this->render('admin_heart_stroke/newManually.html.twig', [
            'heartstroke' => $heartstroke,
            'form' => $form->createView(),
        ]);
    } 

    /**
     * @Route("heartstroke/{id}", name="admin_heartstroke_show", methods="GET")
     */
    public function show(HeartStroke $heartStroke): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('heartstrokes');
            return $this->render('admin_heart_stroke/search.html.twig', [
                    'heartstrokes' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        return $this->render('admin_heart_stroke/show.html.twig', ['heartstroke' => $heartStroke]);
    }
    

    /**
     * @Route("heartstroke/{id}/edit", name="admin_heartstroke_edit", methods="GET|POST")
     */
    public function edit(Request $request, HeartStroke $heartstroke, CategoryRepository $repoCategory, UserRepository $repoUser): Response
    {
        // RECUPERATION DES NOMS DES CATEGORIES  SOUS FORME D'UN TABLEAU A PASSER EN OPTIONS AU FORMULAIRE
        $repoCategory   = $this->getDoctrine()->getRepository(Category::class);
        $categories     = $repoCategory->findAll();
        $users          = $repoUser->findAll();

        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('heartstrokes');
            return $this->render('admin_heart_stroke/search.html.twig', [
                    'heartstrokes' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        
        //CREATION DU FORMULAIRE
        $form = $this->createForm(HeartStrokeEditType::class, $heartstroke, array(
            // DONNEES PASSEES EN OPTION 
            'categories' => $categories,
            'users'      => $users
        ));
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', "Le coup de coeur a bien été modifié !");

            //return $this->redirectToRoute('admin_events_delete', ['id' => $event->getId()]);
            return $this->redirectToRoute('admin_heartstroke');
        }

        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_heart_stroke/edit.html.twig', [
            'heartstroke' => $heartstroke,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("heartstroke/{id}/delete", name="admin_heartstroke_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN') ")
     */
    public function delete(Request $request, HeartStroke $heartStroke): Response
    {
        //Suppression du coup de coeur
        if ($this->isCsrfTokenValid('delete'.$heartStroke->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($heartStroke);
            $em->flush();
            $this->addFlash('success', "Le coup de coeur a bien été supprimé !");
        }

        return $this->redirectToRoute('admin_heartstroke');
     } 
}
