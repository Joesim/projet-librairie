<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Form\EvenementType;
use App\Form\EvenementEditType;
use App\Repository\EvenementRepository;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("admin/")
 * @Security("has_role('ROLE_CONTRIBUTOR') or has_role('ROLE_ADMIN') ")
 */
class AdminEvenementController extends AdminController
{
    /**
     * @Route("evenement", name="admin_evenement")
     */
    public function index(): Response
    {
        //On teste si l'admin effectue une recherche
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('evenements');
            
            return $this->render('admin_evenement/search.html.twig', [
                    'evenements'  => $res['resultat'],
                    'search'  => $res['search'],
                    'page'    => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit'   => $res['limit']
                ]);
        }else{
            //Sinon, on affiche toutes les actualités/événements de la première page
            $res = $this->pagination('evenements');
            
            if(empty($res['resultat'])){
              $this->addFlash('danger', "Il n'y a aucun événement" );
            }
            
            return $this->render('admin_evenement/index.html.twig', [
                    'evenements' => $res['resultat'],
                    'page'      => $res['page'],
                    'maxPage'   => $res['maxPage'],
                    'limit'     => $res['limit']
                ]);
            }
    }

    /**
     * @Route("evenement/new", name="admin_evenement_new", methods="GET|POST")
     */
    public function new(Request $request, EvenementRepository $repo): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('evenement');
            return $this->render('admin_evenement/search.html.twig', [
                    'evenement' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
      
        //CREATION DU FORMULAIRE
        $evenement = new Evenement();
        
        $form = $this->createForm(EvenementType::class, $evenement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $data = $form->getData();

            if($data->getContent() != null)
            {
                $evenement->setCreatedAt($data->getCreatedAt());
    
                $em = $this->getDoctrine()->getManager();
                $em->persist($evenement);
                $em->flush();
                
                $this->addFlash('success', "L'événement a bien été ajouté!");
                return $this->redirectToRoute('admin_evenement');
            }else{
                $this->addFlash('danger', "Le contenu de l'événement ne peut pas être vide!");
            }
        }

        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_evenement/new.html.twig', [
            'evenement' => $evenement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("evenement/{id}", name="admin_evenement_show", methods="GET")
     */
    public function show(Evenement $evenement): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('evenements');
            return $this->render('admin_evenement/search.html.twig', [
                    'evenement' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        
        return $this->render('admin_evenement/show.html.twig', ['evenement' => $evenement]);
    }

    /**
     * @Route("evenement/{id}/edit", name="admin_evenement_edit", methods="GET|POST")
     */
    public function edit(Request $request, Evenement $evenement): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('evenements');
            return $this->render('admin_evenement/search.html.twig', [
                    'evenements' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        
        //CREATION DU FORMULAIRE
        $form = $this->createForm(EvenementEditType::class, $evenement);
        
        //On sauvegarde le titre et le contenu avant de soumettre le formulaire
        $title = $form->getData()->getTitle();
        $content = $form->getData()->getContent();
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ON TEST SI ON A CLIQUE SUR LE BOUTON 'Publier a nouveau'
            $nextAction = $form->get('saveAndAdd')->isClicked() ? 'new_published' : 'admin_evenement';
            
            // SI ON A CLIQUE SUR LE BOUTON 'Modifier'
            if($nextAction == 'admin_evenement'){
                $this->getDoctrine()->getManager()->flush();
                
                $this->addFlash('success', "L'événement a bien été modifié !");
                return $this->redirectToRoute($nextAction);
            }else{
                // SINON SI ON A CLIQUE SUR LE BOUTON 'Publier a nouveau'
                
                //On sauvegarde l'actualité précédente en réécrivant avec les anciennes valeurs
                $evenement->setContent($content)
                          ->setTitle($title);
                $this->getDoctrine()->getManager()->flush();
                
                //On peut alors créer notre nouvelle actualité
                $newEvenement = new Evenement();
                $newForm = $this->createForm(EvenementEditType::class, $newEvenement);
                $newForm->handleRequest($request);
                
                $newEvenement->setCreatedAt(new \DateTime("now",new \DateTimezone("Europe/Paris")));
        
                $em = $this->getDoctrine()->getManager();
                $em->persist($newEvenement);
                $em->flush();
                
                $this->addFlash('success', "L'événement a bien été republié !");
                return $this->redirectToRoute('admin_evenement');
             }
        }
        
        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_evenement/edit.html.twig', [
            'evenement' => $evenement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("evenement/{id}/delete", name="admin_evenement_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN') ")
     */
    public function delete(Request $request, Evenement $evenement): Response
    {
        //Suppression de l''actualité/évenement'
        if ($this->isCsrfTokenValid('delete'.$evenement->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($evenement);
            $em->flush();
            $this->addFlash('success', "L'événement a bien été supprimé !");
        }

        return $this->redirectToRoute('admin_evenement');
    }
}
