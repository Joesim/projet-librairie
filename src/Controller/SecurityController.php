<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Entity\User;
use App\Form\UserSetPasswordType;
use App\Form\UserSetNewPasswordType;
use App\Repository\UserRepository;

class SecurityController 
    extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        

        // REDIRIGER VERS HOME, AVEC MESSAGES D'ERREUR EN FLASH
        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        return new Response('<html><body>logout</body></html>');
    }

    
    
    
    // FONCTION QUI ENVOIE UN MAIL POUR SE CREER UN NOUVEAU MOT DE PASSE
    /**
     * @Route("login/passwordForgotten", name="passwordForgotten", methods="GET|POST")
     */
    public function passwordForgotten(UserRepository $repoUser,  \Swift_Mailer $mailer, Request $request)
    {
        //CREATION DU FORMULAIRE
        $user = new User();
        $form = $this->createForm(UserSetPasswordType::class, $user);
        $form->handleRequest($request);
        
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            // RECUPERE LES DONNEES DU FORMULAIRE
            $emailFrom = 'librairie.motamot.pertuis@gmail.com';
            $emailTo = $form['emailoublie']->getData();
            $token = md5(uniqid(rand())); // token pour l'oublie de mot de passe
            
            // RECUPERE L'UTILISATEUR DE L'EMAIL
            $user = $repoUser->findOneBy(['email' => $emailTo]);
            

            
            if(!$user){
                $this->addFlash('danger', "L'adresse [ $emailTo ] n'est rattaché à aucun compte, veuillez réessayer...");
                return $this->redirectToRoute('passwordForgotten');
            }else{
                
                // ENREGISRTER LE TOKEN EN BASE DE DONNEES
                $user->setToken($token);
                $this->getDoctrine()->getManager()->flush();
                
                // ENVOIE LE MAIL
                $message = (new \Swift_Message('Mot de passe oublié : Librairie Mot à mot'))
                            // ->setSubject($title)
                            // ->setContentType('text/html')
                            // ->setCharset('utf-8')
                            // ->setBody($content)
                            // ->attach(Swift_Attachment::fromPath('my-document.pdf'))
                            // ->setSender('your@address.tld')
                            ->setFrom([$emailFrom])
                            ->setTo([$emailTo,
                                    //  'other@domain.org' => 'A name'
                                     ])
                            ->setBody(  $this->renderView( 'security/registration.html.twig', array(
                                                            'emailTo' => $emailTo,
                                                            'token'   => $token
                                                            )
                                                        ),
                                        'text/html' )
                ;
                            
                $mailer->send($message);
                
                $this->addFlash('success', "Nous avons envoyé un email à l'adresse $emailTo, veuillez contrôler votre messagerie");
                
                return $this->redirectToRoute('login');
            }
        }

    
        return $this->render('security/passwordForgotten.html.twig', [
            // 'users' => $user,
            'form' => $form->createView(),
        ]);
    }
    
    
    
    /**
     * @Route("login/passwordSetNew/{token}", name="passwordSetNew", methods="GET|POST")
     */
    public function passwordSetNew($token, UserRepository $repoUser,  Request $request)
    {
        //CREATION DU FORMULAIRE
        $form = $this->createForm(UserSetNewPasswordType::class, null, array(
                'token' => $token
            ));
        $form->handleRequest($request);
        
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            // RECUPERE LES DONNEES DU FORMULAIRE
            $tokenInput = $form['token']->getData();
            $emailInput = $form['email']->getData();
            $passwordInput = $form['password']->getData();
            $passwordCHKInput = $form['passwordCHK']->getData();
            
            $user = $repoUser->findOneBy(['email' => $emailInput]);
            
            // SI L'EMAIL EXISTE BIEN EN BASE DE DONNEES
            if($user){
                // SI LE TOKEN RETOURNE EST LE MEME QUE CELUI STOCKE
                if($user->getToken() == $tokenInput){
                    
                    if($passwordInput == $passwordCHKInput){
                        $hash = password_hash($passwordInput, PASSWORD_BCRYPT);
                        $user->setHash($hash);
                        $user->setToken(md5(uniqid(rand())));
                        
                        $this->getDoctrine()->getManager()->flush();
                        
                        $this->addFlash('success', "Votre mot de passe a bien été réinitialisé, bon retour parmi nous. Vous pouvez maintenant vous connecter");
                        return $this->redirectToRoute('login');
                    }else{
                        
                        $this->addFlash('danger', "Désolé, les mots de passe indiqués sont différents, veuillez vérifier");
                
                        return $this->redirectToRoute('passwordSetNew', ['token' => $token]);
                    }
                    
                }else{
                    $this->addFlash('danger', "Désolé, la clé de sécurité n'est pas valide, veuillez réessayer...");
                    return $this->redirectToRoute('passwordForgotten');
                }

            }else{
                $this->addFlash('danger', "L'adresse [ $emailTo ] n'est rattaché à aucun compte, veuillez réessayer...");
                return $this->redirectToRoute('login');
            }
        }
    
        return $this->render('security/passwordSetNew.html.twig', [
            // 'users' => $user,
            'form' => $form->createView(),
        ]);
    }
}