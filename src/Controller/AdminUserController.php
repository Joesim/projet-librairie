<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Form\UserAdminEditType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("admin/")
 * @Security("has_role('ROLE_CONTRIBUTOR') or has_role('ROLE_ADMIN') ")
 */
class AdminUserController extends AdminController
{
     /**
     * @Route("user", name="admin_user", methods="GET|POST")
     */
    public function index(): Response
    {
        //On teste si l'admin effectue une recherche
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('users');
            
            return $this->render('admin_user/search.html.twig', [
                    'users' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }else{
             
            //Sinon, on affiche tous les utilisateurs de la première page
            $res = $this->pagination('users');
            
            if(empty($res['resultat'])){
              $this->addFlash('danger', "Il n' y a aucun utilisateur" );
            }
            
            return $this->render('admin_user/index.html.twig', [
                    'users' => $res['resultat'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
            }
    }


    /**
     * @Route("user/new", name="admin_user_new", methods="GET|POST")
     */
    public function new(Request $request, UserRepository $repo): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('users');
            return $this->render('admin_user/search.html.twig', [
                    'users' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
      
        //CREATION DU FORMULAIRE
        $user = new User();
        
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            $data = $form->getData();
            $email = $data->getEmail();
            $username = $data->getUsername();
            $emailSearch = $repo->findOneByEmail($email);
            $usernameSearch = $repo->findOneByUsername($username);
           
            //on teste si le nom d'utilisateur et l'email existe déjà en bdd
            if($emailSearch){
                $this->addFlash('danger', "L'email est déjà présent en base de données -> insertion impossible");
            }
            elseif($usernameSearch){
                $this->addFlash('danger', "Le nom d'utilisateur est déjà connu en base de données -> insertion impossible");
            }
            else if($form->isValid()){ 
                //Si les deux champs précédents sont uniques, on hash le mot de passe
                $password = $data->getHash();
                $passwordHash = password_hash($password, PASSWORD_BCRYPT);
                $user->setHash($passwordHash)
                     ->setSessionID(null)
                     ->setSessionIP(null)
                     ->setCreatedAt(new \DateTime("now",new \DateTimezone("Europe/Paris")))
                     ->setActivated(true);
                     
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                
                $this->addFlash('success', "L'utilisateur a bien été ajouté!");
    
                return $this->redirectToRoute('admin_user');
            }
        }
        
        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_user/new.html.twig', [
            'users' => $user,
            'form' => $form->createView(),
        ]);
    }
    
     /**
     * @Route("user/{id}", name="admin_user_show", methods="GET")
     */
    public function show(User $user): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('users');
            return $this->render('admin_user/search.html.twig', [
                    'users' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        return $this->render('admin_user/show.html.twig', ['user' => $user]);
    }
    
    /**
     * @Route("user/{id}/edit", name="admin_user_edit", methods="GET|POST")
     */
    public function edit(Request $request, User $user, UserRepository $repo): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('users');
            return $this->render('admin_user/search.html.twig', [
                    'users' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        
        //On enregistre les données initiales dans des variables avant la soumission du formulaire
        $prevEmail = $user->getEmail();
        $prevUsername = $user->getUsername();
        $prevFirstName = $user->getFirstName();
        $prevLastName = $user->getLastName();
        $prevUserLevel = $user->getUserLevel();
        $prevPhone = $user->getPhone();
        $prevAddress = $user->getAddress();
        $prevNewsletter = $user->getNewsLetter();
        
        $em = $this->getDoctrine()->getManager();
        
        $form = $this->createForm(UserAdminEditType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //On teste si l'email et/ou l'username existe en bdd
            $email = $form->getData()->getEmail();
            $username = $form->getData()->getUsername();
            $emailSearch = $repo->findOneByEmail($email);
            $usernameSearch = $repo->findOneByUsername($username);
            
            //Si l'email ou l'username existe(nt) déjà , on réinitialise l'utilisateur par ses anciennes données (avant la soumission du formulaire)
            if($emailSearch && ($email != $prevEmail) || $usernameSearch && ($username != $prevUsername) ){
                //Affichage message d'erreurs
                if($emailSearch && ($email != $prevEmail)) $this->addFlash('danger', "L'utilisateur est déjà connu [email] -> modification impossible");
                if($emailSearch && ($email != $prevEmail) && $usernameSearch && ($username != $prevUsername)) $this->addFlash('danger', " + ");
                if($usernameSearch && ($username != $prevUsername)) $this->addFlash('danger', "L'utilisateur est déjà connu [username] -> modification impossible");
                
                //Champs obligatoires
                $user->setEmail($prevEmail)
                     ->setUsername($prevUsername)
                     ->setUserLevel($prevUserLevel);
                
                //Champs optionnels (peuvent être nuls)
                if($prevFirstName != '') $user->setFirstName($prevFirstName);
                if($prevLastName != '') $user->setLastName($prevLastName);
                if($prevPhone != '') $user->setPhone($prevPhone);
                if($prevAddress != '') $user->setAddress($prevAddress);
                if($prevNewsletter != '') $user->setNewsLetter($prevNewsletter);
                
                $this->getDoctrine()->getManager()->flush();
            }
            
            else if($form->isValid()){    
    
                $this->getDoctrine()->getManager()->flush();
    
                $this->addFlash('success', "L'utilisateur a bien été modifié !");
                //return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
                return $this->redirectToRoute('admin_user');
             }
        }

        return $this->render('admin_user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    } 
     

    /**
     * @Route("user/{id}/delete", name="admin_user_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN') ")
     */
    public function delete(Request $request, User $user): Response
    {
        //Suppression de l'utilisateur
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $this->addFlash('success', "L'utilisateur a bien été supprimé !");
        }

        return $this->redirectToRoute('admin_user');
     }
}
