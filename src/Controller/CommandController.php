<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Command;
use App\Entity\Contact;

class CommandController extends Controller
{
    /**
     * @Route("/command/{isbn}/{title}/{author}", options={"expose"=true}, name="command")
     */
    // public function index(Request $request, Command $command): Response
    public function index($isbn, $title, $author,  \Swift_Mailer $mailer)
    {
        // RECUPERER LES INFOS DE L'UTILISATEUR CONNECTE
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $username = $user->getUsername();
        $firstname = $user->getFirstName();
        $lastname = $user->getLastName();
        $emailFrom = $user->getEmail();
        $emailTo = 'contact@mot-a-mot.fr';
        $objectToLib  = "[$isbn] $title ($author)";
        $objectToUser  = "Commande : $title ($author)";
        
        // ENVOYER LES DONNEES DU FORMULAIRE Dans la table commande
        $command = new Command();
        $command->setSentAt(new \DateTime());
        $command->setIsbn($isbn);
        $command->setUser($user);
        $command->setOrders('commande');
        $command->setTitle($title);
        $command->setAuthor($author);
        $em = $this->getDoctrine()->getManager();
        $em->persist($command);
        $em->flush();

        $commandRef = $command->getId();
        
        // ENVOYER LES DONNEES DU FORMULAIRE Dans la table contact (pour le mail)
        $contact = new Contact();
        $contact->setCreatedAt(new \DateTime());
        $contact->setUsername($username);
        $contact->setContent('Pas de message supplementaire');
        $contact->setTitle('Commande [Ref ' . $commandRef . ' ]' . $objectToLib);
        $contact->setEmail($emailFrom);
        $em = $this->getDoctrine()->getManager();
        $em->persist($contact);
        $em->flush();
        
        // ENVOYER LE MAIL A LA LIBRAIRIE
        $messageToLib = (new \Swift_Message('Commande [Ref ' . $commandRef . ' ]' . $objectToLib))
                    // ->setSubject($title)
                    // ->setContentType('text/html')
                    // ->setCharset('utf-8')
                    // ->setBody($content)
                    // ->attach(Swift_Attachment::fromPath('my-document.pdf'))
                    // ->setSender('your@address.tld')
                    ->setFrom([$emailFrom => $username])
                    ->setTo([$emailTo,
                            //  'other@domain.org' => 'A name'
                             ])
                    ->setBody(
                        $this->renderView(
                            'command/mailToLib.html.twig',
                            array(
                                'isbn' => $isbn,
                                'title' => $title,
                                'author' => $author,
                                'emailFrom' => $emailFrom,
                                'username' => $username,
                                'firstname' => $firstname,
                                'lastname' => $lastname,
                                'commandRef' => $commandRef
                            )
                        ),
                        'text/html'
                    )
        ;
        $mailer->send($messageToLib);
        
        
        // ENVOYER LE MAIL A L'UTILISATEUR
        $messageToUser = (new \Swift_Message($objectToUser . ' [ref : ' . $commandRef . ' ]'))
                    ->setFrom([$emailTo => 'Librairie Mot à Mot'])
                    ->setTo([$emailFrom])
                    ->setBody(
                        $this->renderView(
                            'command/mailToUser.html.twig',
                            array(
                                'isbn' => $isbn,
                                'title' => $title,
                                'author' => $author,
                                'emailFrom' => $emailFrom,
                                'username' => $username,
                                'firstname' => $firstname,
                                'lastname' => $lastname,
                                'commandRef' => $commandRef
                            )
                        ),
                        'text/html'
                    )
        ;
        $mailer->send($messageToUser);
        
        // ENVOYER LE MAIL A L'UTILISATEUR
        $this->addFlash('success', "Merci $username, votre commande ( ref N° $commandRef ) a bien été envoyée. Nous vous répondrons dans les plus brefs délais");
        
        return $this->redirectToRoute('home');
        // return $this->render('command/index.html.twig', [

        // ]);
    }
}
