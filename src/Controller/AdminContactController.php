<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;

use App\Entity\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("admin/")
 * @Security("has_role('ROLE_CONTRIBUTOR') or has_role('ROLE_ADMIN') ")
 */
class AdminContactController extends AdminController
{
/**
 * @Route("contact", name="admin_contact")
 */
public function index()
{
    $res = $this->pagination('messages');
        
    if(empty($res['resultat'])){
      $this->addFlash('danger', "Il n'y a aucun message" );
    }

    return $this->render('admin_contact/index.html.twig', [
            'messages' => $res['resultat'],
            'page' => $res['page'],
            'maxPage' => $res['maxPage'],
            'limit' => $res['limit']
        ]);
}
    
    
/**
 * @Route("contact/{id}", name="admin_contact_show", methods="GET")
 */
public function show(Contact $message): Response
{
    return $this->render('admin_contact/show.html.twig', ['message' => $message]);
}
}
