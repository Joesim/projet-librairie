<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;



class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact", methods="GET|POST")
     */
    public function index(Request $request, ContactRepository $contactRepo,  \Swift_Mailer $mailer): Response
    {
        
        // SI L'UTILISATEUR EST CONNECTE, ON REMPLI AUTOMATIQUEMENT DES CHAMPS username et email DU FORMULAIRE DE CONTACT
        if( $this->isGranted('IS_AUTHENTICATED_FULLY') ){
            $user = $this->getUser();
            $session_username = $user->getUsername();
            $session_email    = $user->getEmail();
            $display          = 'display:none';
        }else{
            $session_username = '';
            $session_email    = '';
            $display          = '';
        }
        
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact, array(
                'username' => $session_username,
                'email' => $session_email,
                'display' => $display
            ));
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            // RECUPERER LES DONNEES DU FORMULAIRE
            $username = $form['username']->getData();
            $emailFrom = $form['email']->getData();
            $emailTo = 'contact@mot-a-mot.fr';
            $title = $form['title']->getData();
            $content = $form['content']->getData();

            // ENVOYER LES DONNEES DU FORMULAIRE EN BASE DE DONNEES
            $contact->setCreatedAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            
            // ENVOYER LE MAIL

            $message = (new \Swift_Message($title))
                        // ->setSubject($title)
                        // ->setContentType('text/html')
                        // ->setCharset('utf-8')
                        // ->setBody($content)
                        // ->attach(Swift_Attachment::fromPath('my-document.pdf'))
                        // ->setSender('your@address.tld')
                        ->setFrom([$emailFrom => $username])
                        ->setTo([$emailTo,
                                //  'web@joffrey-simonnet.fr' => 'Joffrey'
                                 ])
                        ->setBody(
                            $this->renderView(
                                'contact/registration.html.twig',
                                array('title' => $title,
                                      'content' => $content,
                                      'emailFrom' => $emailFrom,
                                      'username' => $username
                                )
                            ),
                            'text/html'
                        )
            ;

            $nb = $mailer->send($message);
            if ($nb)
            {
                // $this->addFlash('success', "Votre demande a bien été envoyée");
                $this->addFlash('success', "Votre demande a bien été envoyée (" . date('G:i:s') . ")");
            }
            

            // Pass a variable name to the send() method
            // if (!$mailer->send($message, $failures))
            // {
            //   echo "Failures:";
            //   print_r($failures);
            // }

            // return $this->redirectToRoute('contact');
            // return $this->redirectToRoute('contact-send', array('request' => $request, 'form' => $form));
            // return $this->render('contact/registration.html.twig', [
            //     'content' => $content
            // ]);
        }
        
        //AFFICHAGE DU FORMULAIRE
        return $this->render('contact/index.html.twig', [
            'contact' => $contact,
            'form' => $form->createView()
        ]);
    }
}
    