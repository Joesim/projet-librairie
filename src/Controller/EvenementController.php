<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Evenement;
use \Datetime;

class EvenementController extends Controller
{
    /**
     * @Route("/evenement", name="evenement")
     */
    public function index()
    {
        
        $repoEvenement  = $this->getDoctrine()->getRepository(Evenement::class);
        
        $evenementsNext     = $repoEvenement->findNext();
        $evenementsPrev     = $repoEvenement->findPrev();
        $dateNow = new DateTime('-1 days');
        
        return $this->render('evenement/index.html.twig', [
            'evenementsNext' => $evenementsNext,
            'evenementsPrev' => $evenementsPrev,
            'dateNow' => $dateNow
        ]);
    }
}
