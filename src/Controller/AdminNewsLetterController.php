<?php

namespace App\Controller;

use App\Entity\NewsLetter;
use App\Form\NewsLetterType;
use App\Repository\NewsLetterRepository;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("admin/")
 * @Security("has_role('ROLE_CONTRIBUTOR') or has_role('ROLE_ADMIN') ")
 */
class AdminNewsLetterController extends AdminController
{
    /**
     * @Route("newsletter", name="admin_newsletter", methods="GET|POST")
     */
    public function index(): Response
    {
        //On teste si l'admin effectue une recherche
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('newsletters');
            
            return $this->render('admin_news_letter/search.html.twig', [
                    'newsletters' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }else{
             
            //Sinon, on affiche toutes les newsletters de la première page
            $res = $this->pagination('newsletters');
            
            if(empty($res['resultat'])){
              $this->addFlash('danger', "Il n' y a aucune newsletter" );
            }
            
            return $this->render('admin_news_letter/index.html.twig', [
                    'newsletters' => $res['resultat'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
            }
    }
    

    /**
     * @Route("newsletter/new", name="admin_newsletter_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('newsletters');
            return $this->render('admin_news_letter/search.html.twig', [
                    'newsletters' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
      
        //CREATION DU FORMULAIRE
        $newsletter = new NewsLetter();
        
        $form = $this->createForm(NewsLetterType::class, $newsletter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $user = $this->getUser();
            $data = $form->getData();
            if($data->getContent() != null)
            {
                $newsletter->setCreatedAt(new \DateTime("now",new \DateTimezone("Europe/Paris")))
                           ->setUser($user);
    
                $em = $this->getDoctrine()->getManager();
                $em->persist($newsletter);
                $em->flush();
                
                $this->addFlash('success', "La newsletter a bien été ajoutée!");
                return $this->redirectToRoute('admin_newsletter');
            }
            else{
                 $this->addFlash('danger', "Le contenu de la newsletter ne peut pas être vide!");
            }
        }

        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_news_letter/new.html.twig', [
            'newsletters' => $newsletter,
            'form' => $form->createView(),
        ]);
    }





    /**
     * @Route("newsletter/{id}", name="admin_newsletter_show", methods="GET")
     */
    public function show(NewsLetter $newsLetter): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('newsletters');
            return $this->render('admin_news_letter/search.html.twig', [
                    'newsletters' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        return $this->render('admin_news_letter/show.html.twig', ['newsletter' => $newsLetter]);
    }






    /**
     * @Route("newsletter/{id}/edit", name="admin_newsletter_edit", methods="GET|POST")
     */
    public function edit(Request $request, NewsLetter $newsLetter): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('newsletters');
            return $this->render('admin_news_letter/search.html.twig', [
                    'newsletters' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        
        //CREATION DU FORMULAIRE
        $form = $this->createForm(NewsLetterType::class, $newsLetter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', "La newsletter a bien été modifiée !");

            //return $this->redirectToRoute('admin_events_delete', ['id' => $event->getId()]);
            return $this->redirectToRoute('admin_newsletter');
        }

        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_news_letter/edit.html.twig', [
            'newsletter' => $newsLetter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("newsletter/{id}/delete", name="admin_newsletter_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN') ")
     */
    public function delete(Request $request, NewsLetter $newsLetter): Response
    {
        //Suppression de la newsletter
        if ($this->isCsrfTokenValid('delete'.$newsLetter->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($newsLetter);
            $em->flush();
            $this->addFlash('success', "La newsLetter a bien été supprimée !");
        }

        return $this->redirectToRoute('admin_newsletter');
     }
}
