<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\HeartStroke;
use App\Entity\Category;
use App\Repository\HeartStrokeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;


class HeartStrokeController extends Controller
{
    /**
     * @Route("/heartstroke", name="heart_stroke")
     */
    public function index()
    {
        $repoHeartStroke    = $this->getDoctrine()->getRepository(HeartStroke::class);
        $repoCategories     = $this->getDoctrine()->getRepository(Category::class);
        
        $categories     = $repoCategories->findAll();
        $heartStrokes   = array();

        foreach($categories as $categorie){
            $categorieId   = $categorie->getId();
            $categorieName = $categorie->getName();
            
            $heartStrokesByCategorie    = $repoHeartStroke->findByCategoryOrdered($categorieId);
            $heartStrokeYearsByCategory = $repoHeartStroke->findYearsByCategoryOrdered($categorieId);
            
            $years   = array();
            foreach($heartStrokeYearsByCategory as $oneDate){
                $oneYear =  substr($oneDate['publishedDate'], 0, 4);
                array_push($years, $oneYear );
            }
            
            $years = array_unique($years);

            if($heartStrokesByCategorie){
                $heartStrokesByCategorie[0]->{"categoryName"}   =  $categorieName;
                $heartStrokesByCategorie[0]->{"categoryId"}     =  $categorieId;
                $heartStrokesByCategorie[0]->{"categoryYears"}  =  $years;
                
                array_push($heartStrokes, $heartStrokesByCategorie );
            }

        }

        return $this->render('heart_stroke/displayHeart.html.twig', [
            'heartstrokesAll'   => $heartStrokes,
            'categories'        => $categories
        ]);
    }
    
    
    // REQUETTE AJAX pour l'affichage des coups de coeur au clic sur l'année d'une catégorie
    /**
     * @Route("/heartstroke/next", name="heart_stroke_next")
     */
    public function nextHeartstrokes(Request $request, HeartStrokeRepository $repoHS)
    {

        $catId = $request->request->get('catId');
        $year = $request->request->get('year');
        
        if(!empty($catId))
        {
            // Pour une catégorie on récupère TOUS les coups de coeurs
            // SINON les trois coups de coeur les plus récents
            // SINON les coups de coeur de l'année sélectionnée
            if($year == 'Tout'){
                $heartStrokes = $repoHS->findAllByCategoryOrdered($catId);
            }elseif($year == 'last_three'){
                $heartStrokes = $repoHS->findByCategoryOrdered($catId);
            }else{
                $heartStrokes = $repoHS->findByYearByCategoryOrdered($catId, $year);
            }

            
            foreach ($heartStrokes as $heartStroke){

                $output[]=array(
                    $heartStroke->getId(),
                    $heartStroke->getTitle(),
                    nl2br($heartStroke->getOpinion()),
                    $heartStroke->getCreatedAt()->format('d/m/Y'),
                    $heartStroke->getUser()->getUsername(),
                    nl2br($heartStroke->getDescription()),
                    $heartStroke->getImageLink(),
                    substr($heartStroke->getPublishedDate(), 0, 10),
                    $heartStroke->getPublisher(),
                    $heartStroke->getAuthors(),
                    $heartStroke->getPrice(),
                    $heartStroke->getPriceCurrency(),
                    $heartStroke->getIsbn(),
                );
            }
        }

        return new JsonResponse($output);
    }
    
}
