<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserProfileType;
use App\Repository\UserRepository;
use App\Repository\CommandRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// use Symfony\Component\Security\CoreEncoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

// @Security("has_role('ROLE_ADMIN')")

/**
 * @Route("/moncompte")
 * @Security("has_role('ROLE_USER') or has_role('ROLE_CONTRIBUTOR') or has_role('ROLE_ADMIN') ")
 */
class UserController extends Controller
{

    /**
     * @Route("/", name="user_profil", methods="GET")
     */
    public function index(UserRepository $userRepository, CommandRepository $commandRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $command = $commandRepo->findCommandUser($user);
        
        return $this->render('user/index.html.twig', [
            'commands' => $command]);
    }
    
    /**
     * @Route("/{id}/edit", name="user_edit", methods="GET|POST")
     */
     public function edit(Request $request, User $user, UserRepository $repo): Response
    {
        //On enregistre les données initiales dans des variables avant la soumission du formulaire
        $prevEmail = $user->getEmail();
        $prevUsername = $user->getUsername();
        $prevFirstName = $user->getFirstName();
        $prevLastName = $user->getLastName();
        $prevHash = $user->getHash();
        $prevPhone = $user->getPhone();
        $prevAddress = $user->getAddress();
        $prevNewsletter = $user->getNewsLetter();
        
        $em = $this->getDoctrine()->getManager();
        
        $form = $this->createForm(UserProfileType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //On teste si l'email et/ou l'username existe en bdd
            $email = $form->getData()->getEmail();
            $username = $form->getData()->getUsername();
            $emailSearch = $repo->findOneByEmail($email);
            $usernameSearch = $repo->findOneByUsername($username);
           
            //Si l'email ou l'username existe(nt) déjà , on réinitialise l'utilisateur par ses anciennes données (avant la soumission du formulaire)
            if($emailSearch && ($email != $prevEmail) || $usernameSearch && ($username != $prevUsername) ){
                //Affichage message d'erreurs
                if($emailSearch && ($email != $prevEmail)) $this->addFlash('danger', "Email invalide");
                if($emailSearch && ($email != $prevEmail) && $usernameSearch && ($username != $prevUsername)) $this->addFlash('danger', " + ");
                if($usernameSearch && ($username != $prevUsername)) $this->addFlash('danger', "Nom utilisateur invalide");
                
                //Champs obligatoires
                $user->setEmail($prevEmail)
                     ->setUsername($prevUsername)
                     ->setHash($prevHash);
                
                //Champs optionnels (peuvent être nuls)
                if($prevFirstName != '') $user->setFirstName($prevFirstName);
                if($prevLastName != '') $user->setLastName($prevLastName);
                if($prevPhone != '') $user->setPhone($prevPhone);
                if($prevAddress != '') $user->setAddress($prevAddress);
                if($prevNewsletter != '') $user->setNewsLetter($prevNewsletter);
                
                $this->getDoctrine()->getManager()->flush();
            }
            
            //Sinon, on peut alors faire les modifications de l'utilisateur
            else if($form->isValid()){
                //On n'oublie pas de hashé le mot de passe
                $password = $form->getData()->getHash();
                $passwordHash = password_hash($password, PASSWORD_BCRYPT);
                $user->setHash($passwordHash);
    
                $this->getDoctrine()->getManager()->flush();
    
                $this->addFlash('success', "Vos modifications ont bien été enregistrées !");
                //return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
                return $this->redirectToRoute('user_profil');
             }
        }
        
        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
