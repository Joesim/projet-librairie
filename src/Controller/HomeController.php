<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Events;

use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;


class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $date = new \Datetime();
        
        $repoEvents = $this->getDoctrine()->getRepository(Events::class);
        $lastEvent = $repoEvents->findLast();

        return $this->render('home/index.html.twig', [
            'lastEvent'   => $lastEvent[0]
        ]);
    }
    

    /**
     * @Route("/mentions-legales", name="mentions-legales")
     */
    public function legals()
    {
        return $this->render('home/legals.html.twig');
    }

}
