<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Events;
use App\Form\EventsType;
use App\Form\EventsEditType;
use App\Repository\EventsRepository;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("admin/")
 * @Security("has_role('ROLE_CONTRIBUTOR') or has_role('ROLE_ADMIN') ")
 */
class AdminEventsController extends AdminController
{
    /**
     * @Route("events", name="admin_events", methods="GET|POST")
     */
    public function index(): Response
    {
        //On teste si l'admin effectue une recherche
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('events');
            
            return $this->render('admin_events/search.html.twig', [
                    'events' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }else{
            //Sinon, on affiche toutes les actualités/événements de la première page
            $res = $this->pagination('events');
            
            if(empty($res['resultat'])){
              $this->addFlash('danger', "Il n'y a aucune actualité ou événement" );
            }
            
            return $this->render('admin_events/index.html.twig', [
                    'events' => $res['resultat'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
            }
    }

    /**
     * @Route("events/new", name="admin_events_new", methods="GET|POST")
     */
    public function new(Request $request, EventsRepository $repo): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('events');
            return $this->render('admin_events/search.html.twig', [
                    'events' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
      
        //CREATION DU FORMULAIRE
        $event = new Events();
        
        $form = $this->createForm(EventsType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $user = $this->getUser();
            $data = $form->getData();

            if($data->getContent() != null)
            {
                $event->setCreatedAt(new \DateTime("now",new \DateTimezone("Europe/Paris")))
                      ->setUser($user);
    
                $em = $this->getDoctrine()->getManager();
                $em->persist($event);
                $em->flush();
                
                $this->addFlash('success', "L'événement a bien été ajouté!");
                return $this->redirectToRoute('admin_events');
            }else{
                $this->addFlash('danger', "Le contenu de l'actualité ne peut pas être vide!");
            }
        }

        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_events/new.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("events/{id}", name="admin_events_show", methods="GET")
     */
    public function show(Events $event): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('events');
            return $this->render('admin_events/search.html.twig', [
                    'events' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        
        return $this->render('admin_events/show.html.twig', ['event' => $event]);
    }

    /**
     * @Route("events/{id}/edit", name="admin_events_edit", methods="GET|POST")
     */
    public function edit(Request $request, Events $event): Response
    {
        //On teste si l'admin effectue une recherche (il peut effectuer une recherche de partout)
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $res = $this->searchPagination('events');
            return $this->render('admin_events/search.html.twig', [
                    'events' => $res['resultat'],
                    'search' => $res['search'],
                    'page' => $res['page'],
                    'maxPage' => $res['maxPage'],
                    'limit' => $res['limit']
                ]);
        }
        
        //CREATION DU FORMULAIRE
        $form = $this->createForm(EventsEditType::class, $event);
        
        //On sauvegarde le titre et le contenu avant de soumettre le formulaire
        $title = $form->getData()->getTitle();
        $content = $form->getData()->getContent();
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ON TEST SI ON A CLIQUE SUR LE BOUTON 'Publier a nouveau'
            $nextAction = $form->get('saveAndAdd')->isClicked() ? 'new_published' : 'admin_events';
            
            // SI ON A CLIQUE SUR LE BOUTON 'Modifier'
            if($nextAction == 'admin_events'){
                $this->getDoctrine()->getManager()->flush();
                
                $this->addFlash('success', "L'événement a bien été modifié !");
                return $this->redirectToRoute($nextAction);
            }else{
                // SINON SI ON A CLIQUE SUR LE BOUTON 'Publier a nouveau'
                
                //On sauvegarde l'actualité précédente en réécrivant avec les anciennes valeurs
                $event->setContent($content)
                      ->setTitle($title);
                $this->getDoctrine()->getManager()->flush();
                
                //On peut alors créer notre nouvelle actualité
                $newUser = $this->getUser();
                $newEvent = new Events();
                $newForm = $this->createForm(EventsEditType::class, $newEvent);
                $newForm->handleRequest($request);
                
                $newEvent->setCreatedAt(new \DateTime("now",new \DateTimezone("Europe/Paris")))
                         ->setUser($newUser);
        
                $em = $this->getDoctrine()->getManager();
                $em->persist($newEvent);
                $em->flush();
                
                $this->addFlash('success', "L'événement a bien été republié !");
                return $this->redirectToRoute('admin_events');
             }
        }
        
        //AFFICHAGE DU FORMULAIRE
        return $this->render('admin_events/edit.html.twig', [
            'event' => $event,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("events/{id}/delete", name="admin_events_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN') ")
     */
    public function delete(Request $request, Events $event): Response
    {
        //Suppression de l''actualité/évenement'
        if ($this->isCsrfTokenValid('delete'.$event->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
            $this->addFlash('success', "L'événement a bien été supprimé !");
        }

        return $this->redirectToRoute('admin_events');
    }
}
