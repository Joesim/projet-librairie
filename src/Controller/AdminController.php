<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Events;
use App\Entity\Evenement;
use App\Entity\User;
use App\Entity\HeartStroke;
use App\Entity\NewsLetter;
use App\Entity\Category;
use App\Entity\Contact;

use App\Repository\EvenementRepository;
use App\Repository\UserRepository;
use App\Repository\HeartStrokeRepository;
use App\Repository\NewsLetterRepository;
use App\Repository\CategoryRepository;
use App\Repository\ContactRepository;

use App\Form\UserType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_CONTRIBUTOR') or has_role('ROLE_ADMIN') ")
 */
class AdminController extends Controller
{
    /**
    * @Route("", name="admin_home")
    */
    public function home()
    {
        //Statistiques du site
        $stats = [];
        $repoEvents = $this->getDoctrine()->getRepository(Events::class);
        $repoEvenement = $this->getDoctrine()->getRepository(Evenement::class);
        $repoNewsLetter = $this->getDoctrine()->getRepository(NewsLetter::class);
        $repoHeartStroke = $this->getDoctrine()->getRepository(HeartStroke::class);
        $repoUser = $this->getDoctrine()->getRepository(User::class);
        $repoCategory = $this->getDoctrine()->getRepository(Category::class);
        
        $stats['nbEvents'] = $repoEvents->findNbEvents();
        $stats['nbEvenement'] = $repoEvenement->findNbEvenement();
        $stats['nbNewsLetters'] = $repoNewsLetter->findNbNewsLetters();
        $stats['nbHeartStrokes'] = $repoHeartStroke->findNbHeartStrokes();
        $stats['nbUsers'] = $repoUser->findNbUsers();
        $stats['nbClients'] = $repoUser->getNbUsersByStatut('ROLE_USER');
        $stats['nbContributeurs'] = $repoUser->getNbUsersByStatut('ROLE_CONTRIBUTOR');
        $stats['nbAdmin'] = $repoUser->getNbUsersByStatut('ROLE_ADMIN');
        $stats['nbCategories'] = $repoCategory->findNbHeartCategories();
        
        //Tableau des nombre de coups de coeurs par admin et par catégorie
        $nbHeartStrokeByUser = [];
        $nbHeartStrokeByCategory = [];
        $categories = $repoCategory->findAll();
        $users = $repoUser->getUsersByStatut('ROLE_ADMIN');
        
        foreach($categories as $category){
            $nbHeartStrokeByCategory[$category->getName()] = $repoHeartStroke->findNbHeartStrokesByCategory($category);
            foreach($users as $user){
                if($user->getUsername() != 'Admin'){
                    $oneCategory[$user->getUsername()] = $repoHeartStroke->findNbHeartStrokesByCategoryByUser($user, $category);
                    $nbHeartStrokeByUser[$user->getUsername()] = $repoHeartStroke->findNbHeartStrokesByUser($user);
                }
            }
            $nbHeartStrokeByCategoryByUser[$category->getName()] = $oneCategory;
            $oneCategory = [];
        }
        $stats['nbHeartStrokeByCategory'] = $nbHeartStrokeByCategory;
        $stats['nbHeartStrokeByUser'] = $nbHeartStrokeByUser;
        $stats['nbHeartStrokeByCategoryByUser'] = $nbHeartStrokeByCategoryByUser;

        // dump($nbHeartStrokeByCategory);
        
        return $this->render('admin_home/index.html.twig',[
            'stats' => $stats
            ]);
    }
    
    //METHODE POUR LA PAGINATION GENERALE (utilisée dans les classes filles)
    public function pagination($repository){
        switch($repository){
            case 'events':
                $repo = $this->getDoctrine()->getRepository(Events::class);
                $functionFind = "findNbEvents";
                $functionPagination = "getEventsPagination";
                break;
                
            case 'evenements':
                $repo = $this->getDoctrine()->getRepository(Evenement::class);
                $functionFind = "findNbEvenement";
                $functionPagination = "getEvenementsPagination";
                break;
                
            case 'newsletters':
                $repo = $this->getDoctrine()->getRepository(NewsLetter::class);
                $functionFind = "findNbNewsLetters";
                $functionPagination = "getNewsLettersPagination";
                break;
                
            case 'users':
                $repo = $this->getDoctrine()->getRepository(User::class);
                $functionFind = "findNbUsers";
                $functionPagination = "getUsersPagination";
                break;
                
            case 'heartstrokes':
                $repo = $this->getDoctrine()->getRepository(HeartStroke::class);
                $functionFind = "findNbHeartStrokes";
                $functionPagination = "getHeartStrokePagination";
                break;
                
            case 'messages':
                $repo = $this->getDoctrine()->getRepository(Contact::class);
                $functionFind = "findNbMessages";
                $functionPagination = "getMessagesPagination";
                break;
        }
        
        $page = 1;
        $offset = 0;
        $maxPage = 1;
        
        $limit = trim(strip_tags($_REQUEST["limit"] ?? ""));
        if($limit == '') $limit=50;
        
        $nbResult = $repo->$functionFind();
        
        $maxPage = ceil($nbResult/$limit);
        $page = trim(strip_tags($_REQUEST["page"] ?? ""));
        
        if ($page != '' && ctype_digit($page) && $page > 0 && $page <= $maxPage) 
        {
            $offset = ($page - 1) * $limit;
        }
        else{
            $page = 1;
        }
        
        $result = $repo->$functionPagination($limit, $offset);
        
          return [
                'resultat' => $result,
                 'page' => $page,
                 'maxPage' => $maxPage,
                 'limit' => $limit
            ];
    }
    
    
    //METHODE POUR LA PAGINATION POUR LA RECHERCHE (utilisée dans les classes filles)
    public function searchPagination($repository)
    {
        switch($repository){
            case 'events':
                $repo = $this->getDoctrine()->getRepository(Events::class);
                $functionKeyword = "getEventsByKeyword";
                $name = "actualités";
                break;
                
            case 'evenement':
                $repo = $this->getDoctrine()->getRepository(Evenement::class);
                $functionKeyword = "getEvenementByKeyword";
                $name = "événements";
                break;
                
            case 'newsletters':
                $repo = $this->getDoctrine()->getRepository(NewsLetter::class);
                $functionKeyword = "getNewsLetterByKeyword";
                 $name = "newletters";
                break;
                
            case 'users':
                $repo = $this->getDoctrine()->getRepository(User::class);
                $functionKeyword = "getUserByKeyword";
                $name = "utilisateurs";
                break;
                
            case 'heartstrokes':
                $repo = $this->getDoctrine()->getRepository(HeartStroke::class);
                $functionKeyword = "getHearthStrokeByKeyword";
                $name = "coups de coeur";
                break;
        }

        $page = 1;
        $offset = 0;
        $maxPage = 1;
        
        $limit = trim(strip_tags($_REQUEST["limit"] ?? ""));
        if($limit == '') $limit=50;
        
        //On teste si l'administateur a effectué une recherche 
        $search = trim(strip_tags($_REQUEST["s"] ?? ""));
        if($search != ''){
            $searchResult = $repo->$functionKeyword($search);
            
            if(empty($searchResult)){
              $this->addFlash('danger', "Aucun résultat pour la recherche '$search' dans les $name" );
              $sRes = 0;
            }
            else{
                $result = (count($searchResult) == 1) ? "résultat" : "résultats"; 
                $this->addFlash('success', count($searchResult) . " $result pour la recherche '$search' dans les $name");
                
                $maxPage = ceil(count($searchResult)/$limit);
                $page = trim(strip_tags($_REQUEST["page"] ?? ""));
                if ($page != '' && ctype_digit($page) && $page > 0 && $page <= $maxPage) 
                {
                    $offset = ($page - 1) * $limit;
                }
                else{
                    $page = 1;
                }
                
                if($maxPage == 1){
                    $limit = count($searchResult); 
                }
                if ($page == $maxPage) {
                    $limit = count($searchResult) - $offset;
                }

                for($i=$offset;$i<$offset + $limit; $i++){
                    $sRes[] = $searchResult[$i];
                }
            }
            
            return [
                'resultat' => $sRes,
                 'search' => $search,
                 'page' => $page,
                 'maxPage' => $maxPage,
                 'limit' => $limit
            ];
        }
    }
}
