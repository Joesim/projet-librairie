<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public $emailoublie;

    /**
     * @ORM\Column(name="username", type="string", length=191, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(name="email", type="string", length=191, unique=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hash;

    /**
     * @ORM\Column(type="integer", columnDefinition="INT(10) UNSIGNED ZEROFILL", nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userLevel;

    /**
     * @ORM\Column(type="boolean")
     */
    private $newsLetter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sessionID;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sessionIP;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Events", mappedBy="user")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HeartStroke", mappedBy="user")
     */
    private $heartStrokes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Command", mappedBy="user", orphanRemoval=true)
     */
    private $commands;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsLetter", mappedBy="user", orphanRemoval=true)
     */
    private $newsLetters;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->heartStrokes = new ArrayCollection();
        $this->commands = new ArrayCollection();
        $this->newsLetters = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName = null): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName = null): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(?int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getUserLevel(): ?string
    {
        return $this->userLevel;
    }

    public function setUserLevel(string $userLevel): self
    {
        $this->userLevel = $userLevel;

        return $this;
    }

    public function getNewsLetter(): ?bool
    {
        return $this->newsLetter;
    }

    public function setNewsLetter(bool $newsLetter): self
    {
        $this->newsLetter = $newsLetter;

        return $this;
    }

    public function getSessionID(): ?string
    {
        return $this->sessionID;
    }

    public function setSessionID(?string $sessionID): self
    {
        $this->sessionID = $sessionID;

        return $this;
    }

    public function getSessionIP(): ?string
    {
        return $this->sessionIP;
    }

    public function setSessionIP(?string $sessionIP): self
    {
        $this->sessionIP = $sessionIP;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getActivated(): ?bool
    {
        return $this->activated;
    }

    public function setActivated(bool $activated): self
    {
        $this->activated = $activated;

        return $this;
    }

    /**
     * @return Collection|Events[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Events $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setUser($this);
        }

        return $this;
    }

    public function removeEvent(Events $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getUser() === $this) {
                $event->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HeartStroke[]
     */
    public function getHeartStrokes(): Collection
    {
        return $this->heartStrokes;
    }

    public function addHeartStroke(HeartStroke $heartStroke): self
    {
        if (!$this->heartStrokes->contains($heartStroke)) {
            $this->heartStrokes[] = $heartStroke;
            $heartStroke->setUser($this);
        }

        return $this;
    }

    public function removeHeartStroke(HeartStroke $heartStroke): self
    {
        if ($this->heartStrokes->contains($heartStroke)) {
            $this->heartStrokes->removeElement($heartStroke);
            // set the owning side to null (unless already changed)
            if ($heartStroke->getUser() === $this) {
                $heartStroke->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Command[]
     */
    public function getCommands(): Collection
    {
        return $this->commands;
    }

    public function addCommand(Command $command): self
    {
        if (!$this->commands->contains($command)) {
            $this->commands[] = $command;
            $command->setUser($this);
        }

        return $this;
    }

    public function removeCommand(Command $command): self
    {
        if ($this->commands->contains($command)) {
            $this->commands->removeElement($command);
            // set the owning side to null (unless already changed)
            if ($command->getUser() === $this) {
                $command->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|NewsLetter[]
     */
    public function getNewsLetters(): Collection
    {
        return $this->newsLetters;
    }

    public function addNewsLetter(NewsLetter $newsLetter): self
    {
        if (!$this->newsLetters->contains($newsLetter)) {
            $this->newsLetters[] = $newsLetter;
            $newsLetter->setUser($this);
        }

        return $this;
    }

    public function removeNewsLetter(NewsLetter $newsLetter): self
    {
        if ($this->newsLetters->contains($newsLetter)) {
            $this->newsLetters->removeElement($newsLetter);
            // set the owning side to null (unless already changed)
            if ($newsLetter->getUser() === $this) {
                $newsLetter->setUser(null);
            }
        }

        return $this;
    }
    
    
    // ********************************************
    //       METHODES DEMANDEES PAR L'INTERFACE
    // ********************************************
    
    public function getRoles()
    {
        if ($this->userLevel == 'ROLE_ADMIN')
            return ['ROLE_ADMIN'];
        elseif ($this->userLevel == 'ROLE_USER')
            return ['ROLE_USER'];
        elseif ($this->userLevel == 'ROLE_CONTRIBUTOR')
            return ['ROLE_CONTRIBUTOR'];
        else
            return [];
    }
    
    public function getPassword(): ?string
    {
        return $this->hash;
    }
    
    public function getSalt()
    {
        // The bcrypt and argon2i algorithms don't require a separate salt.
        // You *may* need a real salt if you choose a different encoder.
        return null;
    }
    
    // function getUsername OK
    
    public function eraseCredentials()
    {
    }
    
        
    // ********************************************
    //       SERIALIZABLE
    // ********************************************
    
    /** @see Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->hash,
            $this->userLevel,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->hash,
            $this->userLevel,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    } 
    
}
