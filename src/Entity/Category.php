<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HeartStroke", mappedBy="category")
     */
    private $heartStrokes;

    public function __construct()
    {
        $this->heartStrokes = new ArrayCollection();
    }

    

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|HeartStroke[]
     */
    public function getHeartStrokes(): Collection
    {
        return $this->heartStrokes;
    }

    public function addHeartStroke(HeartStroke $heartStroke): self
    {
        if (!$this->heartStrokes->contains($heartStroke)) {
            $this->heartStrokes[] = $heartStroke;
            $heartStroke->setCategory($this);
        }

        return $this;
    }

    public function removeHeartStroke(HeartStroke $heartStroke): self
    {
        if ($this->heartStrokes->contains($heartStroke)) {
            $this->heartStrokes->removeElement($heartStroke);
            // set the owning side to null (unless already changed)
            if ($heartStroke->getCategory() === $this) {
                $heartStroke->setCategory(null);
            }
        }

        return $this;
    }
}
