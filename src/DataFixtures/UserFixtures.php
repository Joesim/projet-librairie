<?php

namespace App\DataFixtures;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface; // pour encoder le mot de passe

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Events;
use App\Entity\HeartStroke;
use App\Entity\Book;
use App\Entity\Command;
use App\Entity\NewsLetter;
use App\Entity\Category;
use App\Entity\Evenement;

class UserFixtures extends Fixture
{
   
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        $categoryList = array ("littérature française", "littérature étrangère",  "essais", "polars", "littérature adolescent", "jeunesse", "BD - manga");
        // $plainPassword = 'password';
        $books = [];
        $users = [];
        $categories = [];
        
        // Users
        $levelUser = ['ROLE_USER', 'ROLE_CONTRIBUTOR'];
        for($i=0; $i<10; $i++)
        {
            $user = new User();
            // $password = $encoder->encodePassword($user, $plainPassword);
            $user ->setFirstName($faker->firstName)
                  ->setLastName($faker->lastName)
                  ->setUsername($faker->lastName)
                  ->setEmail($faker->email)
                  ->setHash(password_hash('password', PASSWORD_BCRYPT))
                  ->setPhone(mt_rand(100000000,999999999))
                  ->setAddress($faker->address)
                  ->setUserLevel($levelUser[array_rand($levelUser)])
                  ->setNewsLetter(rand(0,1) == 1)
                  ->setToken(null)
                  ->setSessionID(null)
                  ->setSessionIP(null)
                  ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                  ->setActivated(true);
            
            $users[] = $user;
                  
            $manager->persist($user);
        }
        
        $user = new User();
            $user ->setFirstName("Cédric")
                  ->setLastName("DEDENON")
                  ->setUsername("admin")
                  ->setEmail("cedricdedenon@gmail.com")
                  ->setHash(password_hash('admin', PASSWORD_BCRYPT))
                  ->setPhone(123456789)
                  ->setAddress("Aix en provence")
                  ->setUserLevel("ROLE_ADMIN")
                  ->setNewsLetter(true)
                  ->setSessionID(null)
                  ->setSessionIP(null)
                  ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                  ->setActivated(true);
            $manager->persist($user);
        
        $user = new User();
            $user ->setFirstName("Marie")
                  ->setLastName("Pottier")
                  ->setUsername("client")
                  ->setEmail("marie@pottier.com")
                  ->setHash(password_hash('client', PASSWORD_BCRYPT))
                  ->setPhone(123456789)
                  ->setAddress("Aix en provence")
                  ->setUserLevel("ROLE_USER")
                  ->setNewsLetter(true)
                  ->setSessionID(null)
                  ->setSessionIP(null)
                  ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                  ->setActivated(true);
            $manager->persist($user);

        
        // Events (Actualités)
        for($i=0; $i<10; $i++)
        {
            $event = new Events();
            $event ->setTitle( $faker->sentence(mt_rand(4, 8)))
                  ->setContent('<p>' . implode('</p><p>', $faker->paragraphs(2)) . '</p>')
                  ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                  ->setUser($user);
                  
            $manager->persist($event);
        }
        
        
        // Evenements
        for($i=0; $i<10; $i++)
        {
            $evenement = new Evenement();
            $evenement ->setTitle( $faker->sentence(mt_rand(4, 8)))
                  ->setContent('<p>' . implode('</p><p>', $faker->paragraphs(2)) . '</p>')
                  ->setCreatedAt($faker->dateTimeBetween('-6 months'));
                  
            $manager->persist($evenement);
        }
        
        
        //Categories
        for($i=0; $i < count ($categoryList); $i++)
        {
            $category   = new Category();
            $category   ->setName( $categoryList[$i]);
            
            $categories[] = $category ;

            $manager->persist($category);
        }
        
        //HeartStrokes
        for($i=0; $i<50; $i++)
        {
            $heartStroke = new HeartStroke();
            $heartStroke ->setTitle($faker->sentence(mt_rand(4, 8)))
                         ->setOpinion('<p>' . implode('</p><p>', $faker->paragraphs(2)) . '</p>')
                         ->setCategory($categories[array_rand($categories)])
                         ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                         ->setUser($user)
                         ->setImageLink($faker->imageUrl(300, 400, 'city'))
                         ->setDescription('<p>' . implode('</p><p>', $faker->paragraphs(2)) . '</p>')
                         ->setPublishedDate(mt_rand(1900,2018))
                         ->setPublisher($faker->lastName)
                         ->setAuthors($faker->lastName)
                         ->setPrice(null)
                         ->setPriceCurrency(null)
                         ->setIsbn(mt_rand(1000000000000,9999999999999))
                         ->setPages(mt_rand(10,1000));
                  
            $manager->persist($heartStroke);
        }
        
        //Command
        $orders = ['commande', 'reservation'];
        for($i=0; $i<100; $i++)
        {
            $command = new Command();
            $command->setIsbn(mt_rand(1000000000000,9999999999999))
                    ->setTitle($faker->sentence(mt_rand(2, 6)))
                    ->setAuthor($faker->lastName)
                    ->setUser($users[array_rand($users)])
                    ->setSentAt($faker->dateTimeBetween('-1 hours'))
                    ->setOrders($orders[array_rand($orders)]);
                     
            $manager->persist($command);
        }
        
        //Newsletter
        for($i=0; $i<10; $i++)
        {
            $newsletter = new NewsLetter();
            $newsletter ->setTitle( $faker->sentence(mt_rand(4, 8)))
                        ->setContent('<p>' . implode('</p><p>', $faker->paragraphs(2)) . '</p>')
                        ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                        ->setUser($user);
                     
            $manager->persist($newsletter);
        }
        
        $manager->flush();
    }
}
