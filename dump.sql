-- MySQL dump 10.13  Distrib 5.5.57, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: librairie
-- ------------------------------------------------------
-- Server version	5.5.57-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evenement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evenement`
--

LOCK TABLES `evenement` WRITE;
/*!40000 ALTER TABLE `evenement` DISABLE KEYS */;
INSERT INTO `evenement` VALUES (14,'Enim eos aspernatur molestiae numquam sequi tenetur inventore rerum nemo ea eos.','<p>Quidem sequi adipisci laudantium sint. Similique quibusdam eum aut eligendi. Vero itaque consequatur sapiente porro quibusdam.</p><p>Dolorum quos quasi numquam dolore aspernatur ullam autem. Quas adipisci eum explicabo nisi. Corrupti et et mollitia non aut aliquid ratione dolorem.</p>','2018-05-27 05:41:38'),(15,'Iusto dolor mollitia voluptatem in vitae.','<p>Saepe architecto sunt molestiae qui placeat placeat impedit. Sunt in quia dolore sit sequi maxime optio magni. Ducimus culpa inventore nam distinctio accusantium omnis.</p><p>Perspiciatis quasi ea aut deleniti facere fugit ex inventore. Ducimus aut quis expedita nihil rerum dolores deleniti voluptas. In atque ea a et porro velit odio. Est modi error id.</p>','2018-06-08 23:03:03'),(16,'Accusamus est ullam ipsam ut.','<p>Laudantium ut nesciunt fuga omnis. Excepturi quis unde et debitis suscipit. Natus qui velit et. Vitae consequatur et iusto ad.</p><p>Qui voluptatem qui cum ullam voluptas explicabo adipisci. Voluptas iste praesentium esse quos quisquam dolores tempore. Commodi nobis quod temporibus harum.</p>','2018-07-05 19:00:19'),(17,'Sit sit et voluptates consequatur possimus quasi soluta aliquam modi.','<p>Aliquam eos aspernatur reiciendis velit alias. Sit et vel repudiandae inventore aut. Illo perspiciatis consequatur adipisci enim facilis.</p><p>Autem fuga qui ea qui cumque autem reiciendis. Earum omnis cum reiciendis a quidem voluptatem id iure. A hic ducimus illum et error eum quod. Laudantium eligendi pariatur ducimus quo et doloremque id nobis.</p>','2018-08-23 23:47:58'),(18,'Molestiae sit placeat incidunt voluptates consequatur.','<p>Iste nulla error voluptatem quod et. Nostrum ut odit asperiores beatae eaque qui. Ut porro ut quasi tempora maiores eos.</p><p>Quisquam voluptatum in eius dolores illum dolor. Sapiente et ut a quis animi quisquam.</p>','2018-07-01 18:57:44'),(19,'Deserunt et nulla amet sit.','<p>Repellat optio corrupti quas at. Sapiente consequatur blanditiis eum suscipit qui voluptatem voluptatem.</p><p>Deleniti optio culpa laboriosam voluptatem. Iure cupiditate nam sint sed maxime error officia. Facilis dolore est accusantium aut. Nam suscipit voluptas ut sit ab itaque. Quia ipsam numquam est et.</p>','2018-08-05 10:48:44'),(20,'Praesentium quia et fugit exercitationem dignissimos.','<p>Ex minima et ea quaerat. Saepe mollitia provident enim ullam officia. Illum enim minima earum odit.</p><p>Ut ut sunt ipsam ipsa aliquid illo deserunt ut. Non voluptates ut illum aut deserunt eum. Ut possimus dolorem quisquam dolorum enim. Et nam sit eum sed qui assumenda cumque.</p>','2018-08-05 18:18:35'),(21,'Quasi et repudiandae veniam error.','<p>Voluptatem cupiditate debitis amet nesciunt ipsum enim. Velit soluta officia qui saepe nostrum fugiat quia. Quo vel fugiat laborum ad et.</p><p>Nemo delectus voluptas fugiat est et. Voluptatem id blanditiis omnis iure consectetur. Qui voluptas nisi quam at magnam accusamus. Adipisci dicta repellendus placeat aperiam dolor debitis molestiae.</p>','2018-07-04 19:00:42'),(22,'Reiciendis voluptas autem ut ipsam inventore ducimus.','<p>Mollitia at consequatur vel et nisi. Corporis ipsam et officiis provident reprehenderit. Placeat delectus veritatis et et.</p><p>Optio explicabo sunt quas id. Velit voluptatem beatae odit. Voluptas id ea quaerat.</p>','2018-04-18 18:02:45'),(23,'Est enim accusantium quis molestiae.','<p>Sint sint quia tempore repellat est quaerat. Neque laboriosam et quaerat perferendis. Sed laudantium excepturi quaerat corrupti molestiae qui eos.</p><p>Dolor dignissimos est quis tempora unde. Porro aliquam quod labore tempora. Ut officiis perspiciatis sint illo sint.</p>','2018-07-13 16:03:22');
/*!40000 ALTER TABLE `evenement` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-16 22:11:04
